import axiosClient from '../libs/axiosClient';

export const getSingleClient = async (id) => {
  try {
    const res = await axiosClient({
      url: `/clients/getSingle/${id}`
    });
    return res.data.client;
  } catch (err) {
    throw err.response.data.error;
  }
};
