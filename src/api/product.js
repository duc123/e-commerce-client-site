import axiosClient from '../libs/axiosClient';

export const getProduct = async (id) => {
  try {
    const res = await axiosClient({
      url: `/products/getSingle/${id}`,
      method: 'GET'
    });
    return res.data.product;
  } catch (err) {
    throw err.response.data.error;
  }
};

export const getByCondition = async (query) => {
  try {
    const res = await axiosClient({
      url: `/products/getByCondition`,
      method: 'GET',
      params: query
    });
    return res.data;
  } catch (err) {
    throw err.response.data.error;
  }
};
