import axios from 'axios';

const axiosClient = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    Accept: 'application/json',
    'Content-type': 'application/json'
  }
});

axiosClient.interceptors.request.use(
  (config) => {
    if (!config.headers.Authorization) {
      const authToken = localStorage.getItem('authToken');
      if (authToken) {
        const token = JSON.parse(authToken);
        config.headers.Authorization = `Bearer ${token}`;
      }
    }
    return config;
  },
  (error) => Promise.reject(error)
);

// Add a response interceptor
axiosClient.interceptors.response.use(
  (response) => {
    if (response && response.data) {
      return response.data;
    }
    return response;
  },
  (err) => {
    // Handle error
    throw err;
  }
);

export default axiosClient;
