import { ThemeProvider } from '@mui/material';
import React from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import Account from './components/Account';
import AppContainer from './components/AppContainer';
import NotFound from './components/NotFound';
import ProtectedRoute from './components/ProtectedRoute';
import theme from './constant/theme';
import EditAddress from './pages/Account/EditAddress';
import EditInfo from './pages/Account/EditInfo';
import Orders from './pages/Account/Orders';
import Notifications from './pages/Account/Notifications';
import CartPage from './pages/CartPage';
import Checkout from './pages/Checkout';
import Payment from './pages/Checkout/Payment';
import Shipping from './pages/Checkout/Shipping';
import Home from './pages/Home';
import Login from './pages/Login';
import Product from './pages/Product';
import Register from './pages/Register';
import NotificationSystem from './components/NotificationSystem';
import ProductsByCondition from './pages/ProductsByCondition';
import ResetPassword from './pages/ResetPassword';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <NotificationSystem />
        <Routes>
          {/* Protected route */}
          <Route element={<ProtectedRoute />}>
            {/* Account */}
            <Route path="account" element={<Account />}>
              <Route path="info" element={<EditInfo />} />
              <Route path="address" element={<EditAddress />} />
              <Route path="orders" element={<Orders />} />
              <Route path="notifications" element={<Notifications />} />
              <Route path="" element={<Navigate replace to="/account/info" />} />
            </Route>
            <Route path="checkout" element={<Checkout />}>
              <Route path="shipping" element={<Shipping />} />
              <Route path="payment" element={<Payment />} />
              <Route path="" element={<Navigate replace to="shipping" />} />
            </Route>
          </Route>
          {/* Protected route */}

          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="reset-password" element={<ResetPassword />} />

          <Route
            path="/"
            element={
              <AppContainer>
                <Home />
              </AppContainer>
            }
          />
          <Route
            path="/products/single/:id"
            element={
              <AppContainer>
                <Product />
              </AppContainer>
            }
          />
          <Route
            path="/carts"
            element={
              <AppContainer>
                <CartPage />
              </AppContainer>
            }
          />
          <Route
            path="/products/condition"
            element={
              <AppContainer>
                <ProductsByCondition />
              </AppContainer>
            }
          />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
};

export default App;
