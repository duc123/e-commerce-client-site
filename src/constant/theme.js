import { createTheme } from '@mui/material';

const theme = createTheme({
  shadows: {
    0: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    1: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    2: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',

    3: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    4: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    5: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    6: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    7: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    8: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    9: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    10: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    11: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    12: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    13: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    14: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    15: 'rgba(3, 0, 71, 0.09) 0px 8px 45px',
    16: 'rgba(3, 0, 71, 0.09) 0px 8px 45px'
  },
  palette: {
    primary: {
      light: '#66B3FF',
      main: '#3391FF',
      dark: '#2570DB'
    },
    success: {
      light: '#85D559',
      main: '#53BA2A',
      dark: '#3A9F1E'
    },
    info: {
      light: '#8A9FFF',
      main: '#637DFF',
      dark: '#485EDB'
    },
    danger: {
      light: '#FF7575',
      main: '#FF4759',
      dark: '#DB3353'
    },

    neutral: {
      main: '#212b36',
      light: 'red',
      contrastText: '#fff'
    }
  },
  typography: {
    fontFamily: 'Montserrat, sans-serif'
  }
});

export default theme;
