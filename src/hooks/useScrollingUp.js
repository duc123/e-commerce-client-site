import { useEffect, useState } from 'react';
import { off, on } from '../utils/scroll';
/**
 * useScrollingUp
 * @returns {boolean}
 */
const useScrollingUp = () => {
  let prevScroll;
  //if it is SSR then check you are now on the client and window object is available
  if (typeof window !== 'undefined') {
    prevScroll = window.pageYOffset;
  }
  const [scrollingUp, setScrollingUp] = useState(true);
  const handleScroll = () => {
    const currScroll = window.pageYOffset;
    if (window.scrollY < 250) {
      setScrollingUp(true);
    } else {
      const isScrolled = prevScroll > currScroll;
      setScrollingUp(isScrolled);
    }
    prevScroll = currScroll;
  };
  useEffect(() => {
    on(window, 'scroll', handleScroll, { passive: true });
    return () => {
      off(window, 'scroll', handleScroll, { passive: true });
    };
  }, []);
  return scrollingUp;
};

export default useScrollingUp;
