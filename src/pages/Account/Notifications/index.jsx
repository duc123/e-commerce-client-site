import { Alert } from '@mui/material';
import React from 'react';

const Notifications = () => {
  return (
    <div>
      <Alert variant="filled" severity="info" sx={{ width: '100%' }}>
        Notification function is not ready.
      </Alert>{' '}
    </div>
  );
};

export default Notifications;
