import { Alert, Chip, Collapse, Pagination } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate, useSearchParams } from 'react-router-dom';
import CustomizeButton from '../../../components/CustomizeButton';
import Loading from '../../../components/Loading';
import axiosClient from '../../../libs/axiosClient';
import { addNoti } from '../../../redux/notiSlice/slice';
import { formatCurrency } from '../../../utils/price';
import style from './style.module.scss';

const OrderPagination = ({ page, count, handleChange }) => {
  return (
    <div className={style.pagi}>
      <Pagination count={count} page={+page} onChange={handleChange} />
    </div>
  );
};

const Order = ({ item }) => {
  const {
    title,
    price,
    quantity,
    image,
    variant,
    attributes,
    deliveryStatus,
    created_at,
    shipping: { city, district, ward, phoneNumber, detail }
  } = item;

  const [shippingVisibility, setShippingVisibility] = useState(false);

  const showShipping = () => setShippingVisibility(true);
  const hideShipping = () => setShippingVisibility(false);

  const handleClick = () => setShippingVisibility(!shippingVisibility);

  const getVariant = () => {
    const keys = attributes.map((el) => el.key);
    return keys.map((k) => (
      <span key={k} className={style.attr}>
        ({variant[k]})
      </span>
    ));
  };

  const getDeliveryStatus = () => {
    return (
      <Chip
        sx={{ fontSize: '11px' }}
        size="small"
        label={deliveryStatus ? 'Success delivery' : 'Processing'}
        color={deliveryStatus ? 'success' : 'warning'}
      />
    );
  };

  const getOrderDate = () => {
    const time = new Date(created_at);
    return `${time.getMonth() + 1}/${time.getDate()}/${time.getFullYear()}`;
  };

  return (
    <div className={style.orderItem}>
      <div className={style.flex}>
        {getOrderDate()}
        {getDeliveryStatus()}
      </div>
      <div className={style.flex}>
        <div className={style.left}>
          <img src={image} />
          <div className={style.titleContainer}>
            <p className={style.title}>
              {title} {variant && getVariant()}
            </p>
            <p>
              {formatCurrency(price)}$ x {quantity}
            </p>
          </div>
        </div>

        <p>{formatCurrency(price * quantity)}$</p>
      </div>
      <CustomizeButton
        onClick={handleClick}
        title={shippingVisibility ? 'Hide shipping' : 'View shipping'}
        type="button"
        size="xs"
      />
      <Collapse in={shippingVisibility}>
        <div className={style.shippingContainer}>
          <div className={style.shipping}>
            <p>Phone number</p>
            <p>{phoneNumber}</p>
          </div>
          <div className={style.shipping}>
            <p>City:</p>
            <p>{city}</p>
          </div>
          <div className={style.shipping}>
            <p>District:</p>
            <p>{district}</p>
          </div>
          <div className={style.shipping}>
            <p>Ward:</p>
            <p>{ward}</p>
          </div>
          <div className={style.shipping}>
            <p>Address:</p>
            <p>{detail}</p>
          </div>
        </div>
      </Collapse>
    </div>
  );
};

const Orders = () => {
  const [orders, setOrders] = useState([]);

  const [pageCount, setPageCount] = useState(1);
  const [current, setCurrent] = useState(1);

  const dispatch = useDispatch();

  const [searchParams] = useSearchParams();

  const p = searchParams.get('p');

  const page = p > 1 ? +p : 1;

  const navigate = useNavigate();

  const [loading, setLoading] = useState(false);

  const getOrders = async (page) => {
    setLoading(true);
    try {
      const res = await axiosClient({
        url: `/orders/getClient?p=${page}`,
        method: 'GET'
      });
      const {
        data: { orders: list, pageCount }
      } = res;
      setOrders(list);
      setPageCount(pageCount);
    } catch (err) {
      dispatch(addNoti(err.response.data.error));
    }
    setLoading(false);
  };

  useEffect(() => {
    getOrders(page);
  }, [page]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const handleChange = (e, num) => {
    navigate(`/account/orders/?p=${num}`);
  };

  return (
    <div>
      {loading ? (
        <Loading loading={loading} />
      ) : (
        <>
          {orders.length > 0 ? (
            <>
              {orders.map((el) => (
                <Order item={el} key={el._id} />
              ))}
              <OrderPagination count={pageCount} page={page} handleChange={handleChange} />
            </>
          ) : (
            <Alert variant="filled" severity="info" sx={{ width: '100%' }}>
              You do not have any order yet.
            </Alert>
          )}
        </>
      )}
    </div>
  );
};

export default Orders;
