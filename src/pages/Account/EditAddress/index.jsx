import React, { useEffect, useState } from 'react';
import style from './style.module.scss';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import CustomizeButton from '../../../components/CustomizeButton';
import CustomizeInput from '../../../components/CustomizeInput';
import { useDispatch, useSelector } from 'react-redux';
import CustomizeSelect from '../../../components/CustomizeSelect';
import { getCities, getDistricts, getWards } from '../../../utils/getProvince';
import { updateClientAddress } from '../../../redux/clientSlice/api';
import CustomizeModal from '../../../components/CustomizeModal';

const EditAddress = () => {
  const dispatch = useDispatch();

  const [modal, setModal] = useState(false);

  const openModal = () => setModal(true);
  const closeModal = () => setModal(false);

  const schema = yup.object().shape({
    city: yup.string().required(),
    district: yup.string().required(),
    ward: yup.string().required(),
    detail: yup.string().required(),
    phone: yup
      .string()
      .required()
      .matches(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/, 'Invalid phone number')
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const { handleSubmit, control, watch, setValue } = useForm({
    resolver: yupResolver(schema)
  });

  const [cities, setCities] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);

  const watchCity = watch('city');
  const watchDistrict = watch('district');

  const [wardCount, setWardCount] = useState(0);
  const [districtCount, setDistrictCount] = useState(0);

  const getCitiesNames = () => (cities.length > 0 ? cities.map((el) => el['province_name']) : []);
  const getDistrictsNames = () =>
    districts.length > 0 ? districts.map((el) => el['district_name']) : [];

  const getWardsNames = () => (wards.length > 0 ? wards.map((el) => el['ward_name']) : []);

  const getCityId = () =>
    cities.length > 0
      ? cities.find((el) => el['province_name'] === watch('city'))?.province_id
      : '';

  const getDistrictId = () =>
    districts.length > 0
      ? districts.find((el) => el['district_name'] === watch('district'))?.district_id
      : '';

  const { client, isLoading } = useSelector((state) => state.clientReducer);

  const submit = async (data) => {
    const res = await dispatch(updateClientAddress(data));
    if (res.payload) openModal();
  };

  // Fetch cities list
  useEffect(() => {
    getCities().then((data) => setCities(data.results));
  }, []);

  // Fetch district list
  useEffect(() => {
    if (watchCity) {
      const cityId = getCityId();
      getDistricts(cityId).then((data) => setDistricts(data.results));
    } else {
      setDistricts([]);
    }
  }, [watchCity]);

  // Fetch ward list
  useEffect(() => {
    if (watchDistrict) {
      const districtId = getDistrictId();
      getWards(districtId).then((data) => setWards(data.results));
    } else {
      setWards([]);
    }
  }, [watchDistrict]);

  useEffect(() => {
    if (client) {
      const {
        address: { city }
      } = client;
      if (cities.length > 0 && city) setValue('city', city);
    }
  }, [client, cities]);

  // Rest district when city changes

  useEffect(() => {
    setValue('district', '');
  }, [watchCity]);

  // Set values initially
  useEffect(() => {
    if (client) {
      const {
        address: { district, phone, detail }
      } = client;
      setValue('phone', phone);
      setValue('detail', detail);
    }
  }, [client]);

  // Set district initially

  useEffect(() => {
    if (client) {
      const {
        address: { district }
      } = client;
      // Only set district on time after client is defined
      if (districts.length > 0 && districtCount <= 0) {
        setValue('district', district);
        setDistrictCount(districtCount + 1);
      }
    }
  }, [client, districts]);

  //Set ward initially
  useEffect(() => {
    if (client) {
      const {
        address: { ward }
      } = client;
      // Only set ward on time after client is defined
      if (wards.length > 0 && wardCount <= 0) {
        setValue('ward', ward);
        setWardCount(wardCount + 1);
      }
    }
  }, [client, wards]);

  // Reset ward when district changes
  useEffect(() => {
    setValue('ward', '');
  }, [watchDistrict]);

  const Content = () => {
    return <div>Your address is updated.</div>;
  };

  return (
    <div>
      <CustomizeModal handleClose={closeModal} open={modal} content={Content} />
      <div className={style.container}>
        <form onSubmit={handleSubmit(submit)}>
          <div className={style.block}>
            <span className={style.label}>Phone number</span>

            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth={true}
                  value={value}
                  onChange={onChange}
                  error={error}
                  type="text"
                  size="sm"
                />
              )}
              name="phone"
              control={control}
            />
          </div>
          {/* City */}
          <div className={style.block}>
            <span className={style.label}>City</span>
            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => {
                return (
                  <CustomizeSelect
                    size="md"
                    value={value}
                    onChange={onChange}
                    error={error}
                    options={getCitiesNames()}
                    disabled={cities.length <= 0 ? true : false}
                    placeholder="Chose a city"
                  />
                );
              }}
              name="city"
              control={control}
            />
          </div>
          {/* District */}
          <div className={style.block}>
            <span className={style.label}>District</span>
            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => {
                return (
                  <CustomizeSelect
                    size="md"
                    value={value}
                    onChange={onChange}
                    error={error}
                    options={getDistrictsNames()}
                    disabled={districts.length <= 0 ? true : false}
                    placeholder="Chose a district"
                  />
                );
              }}
              name="district"
              control={control}
            />
          </div>
          {/* District */}
          <div className={style.block}>
            <span className={style.label}>Ward</span>
            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => {
                return (
                  <CustomizeSelect
                    size="md"
                    value={value}
                    onChange={onChange}
                    error={error}
                    options={getWardsNames()}
                    disabled={districts.length <= 0 ? true : false}
                    placeholder="Chose a ward"
                  />
                );
              }}
              name="ward"
              control={control}
            />
          </div>
          <div className={style.block}>
            <span className={style.label}>Address detail</span>

            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth={true}
                  value={value}
                  onChange={onChange}
                  error={error}
                  type="text"
                  size="sm"
                  multiline={true}
                  rows={4}
                />
              )}
              name="detail"
              control={control}
            />
          </div>
          <div className={style.btnBlock}>
            <CustomizeButton
              loading={isLoading}
              type="submit"
              title="Save"
              color="primary"
              variant="contained"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditAddress;
