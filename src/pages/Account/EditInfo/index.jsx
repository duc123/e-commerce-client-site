import React, { useEffect, useState } from 'react';
import style from './style.module.scss';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import CustomizeButton from '../../../components/CustomizeButton';
import CustomizeInput from '../../../components/CustomizeInput';
import { useDispatch, useSelector } from 'react-redux';
import { updateClientGeneralInfo } from '../../../redux/clientSlice/api';
import CustomizeSelect from '../../../components/CustomizeSelect';
import { Input } from '@mui/material';
import { AddAPhotoOutlined } from '@mui/icons-material';
import { getBase64 } from '../../../utils/toBase64';
import CustomizeModal from '../../../components/CustomizeModal';

const EditInfo = () => {
  const dispatch = useDispatch();

  const [modal, setModal] = useState(false);

  const openModal = () => setModal(true);
  const closeModal = () => setModal(false);

  const [displayedAvatar, setDisplayedAvatar] = useState('');

  const schema = yup.object().shape({
    firstName: yup.string(),
    lasttName: yup.string(),
    gender: yup.string(),
    avatar: yup.mixed()
  });

  const { handleSubmit, control, watch, setValue } = useForm({
    resolver: yupResolver(schema)
  });

  const watchAvatar = watch('avatar');

  const { client, isLoading } = useSelector((state) => state.clientReducer);

  const submit = (data) => {
    const formData = new FormData();
    const keys = Object.keys(data);
    const values = Object.values(data);
    keys.forEach((key, i) => formData.append(key, values[i]));

    sendRequest(formData);
  };

  const sendRequest = async (data) => {
    const res = await dispatch(updateClientGeneralInfo(data));
    if (res.payload) openModal();
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    if (client) {
      const { firstName, lastName, gender, avatar } = client;
      setValue('firstName', firstName);
      setValue('lastName', lastName);
      setValue('gender', gender);
      if (avatar) setDisplayedAvatar(avatar);
    }
  }, [client]);

  useEffect(() => {
    if (watchAvatar) {
      getBase64(watchAvatar).then((value) => setDisplayedAvatar(value));
    } else {
      if (client && !client.avatar) setDisplayedAvatar('');
    }
  }, [watchAvatar, client]);

  const Content = () => {
    return <div>Your address is updated.</div>;
  };

  return (
    <div>
      <CustomizeModal handleClose={closeModal} open={modal} content={Content} />

      <div className={style.container}>
        <form onSubmit={handleSubmit(submit)}>
          <div className={style.block}>
            <span className={style.label}>Avatar</span>
            <label htmlFor="contained-button-file">
              <Controller
                defaultValue=""
                render={({ field: { onChange, value }, fieldState: { error } }) => {
                  return (
                    <>
                      <Input
                        accept="image/*"
                        type="file"
                        value={value.filename}
                        onChange={(e) => {
                          onChange(e.target.files[0]);
                        }}
                        placeholder="Avatar"
                        className={style.file_input}
                        id="contained-button-file"
                      />
                      <p>{error && error.message}</p>
                    </>
                  );
                }}
                name="avatar"
                control={control}
              />
              {displayedAvatar ? (
                <img className={style.avatar} src={displayedAvatar} />
              ) : (
                <div className={style.empty}>
                  <AddAPhotoOutlined />
                </div>
              )}
            </label>
          </div>
          <div className={style.block}>
            <span className={style.label}>Email</span>

            <CustomizeInput disabled={true} fullWidth={true} value={client?.email} type="text" />
          </div>
          <div className={style.block}>
            <span className={style.label}>First name</span>
            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth={true}
                  value={value}
                  onChange={onChange}
                  error={error}
                  type="text"
                />
              )}
              name="firstName"
              control={control}
            />
          </div>
          <div className={style.block}>
            <span className={style.label}>Last name</span>

            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth={true}
                  value={value}
                  onChange={onChange}
                  error={error}
                  type="text"
                />
              )}
              name="lastName"
              control={control}
            />
          </div>
          <div className={style.block}>
            <span className={style.label}>Gender</span>

            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => {
                return (
                  <CustomizeSelect
                    size="md"
                    value={value}
                    onChange={onChange}
                    error={error}
                    options={['male', 'female']}
                  />
                );
              }}
              name="gender"
              control={control}
            />
          </div>
          <div className={style.btnBlock}>
            <CustomizeButton
              loading={isLoading}
              type="submit"
              title="Save"
              color="primary"
              variant="contained"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditInfo;
