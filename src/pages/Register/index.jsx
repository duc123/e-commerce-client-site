import React, { useEffect, useState } from 'react';
import AuthFormContainer from '../../components/AuthFormContainer';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import CustomizeInput from '../../components/CustomizeInput';
import style from './style.module.scss';
import { VisibilityOffOutlined, VisibilityOutlined } from '@mui/icons-material';
import { Alert, IconButton } from '@mui/material';
import CustomizeButton from '../../components/CustomizeButton';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { removeRegisterError } from '../../redux/registerSlice';
import { registerRequest } from '../../redux/registerSlice/api';
import { onRegisterSucceed } from '../../redux/loginSlice';

const Register = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { isLoading, error } = useSelector((state) => state.registerReducer);

  const { authToken } = useSelector((state) => state.loginReducer);

  const schema = yup.object().shape({
    email: yup.string().required('Email can not be blank').email('Invalid email'),
    password: yup.string().required('Password can not be blank').min(6),
    password_confirm: yup
      .string()
      .required()
      .oneOf([yup.ref('password')], 'Password confirm must be the same as password')
  });

  const {
    handleSubmit,
    control,
    formState: { isDirty, isValid }
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'all'
  });

  const submit = async (data) => {
    const res = await dispatch(registerRequest(data));
    if (res.payload) {
      const {
        data: { token }
      } = res.payload;
      dispatch(onRegisterSucceed(token));
    }
  };

  const [passType, setPassType] = useState('password');
  const [confirmType, setConfirmType] = useState('password');

  const changePassType = () => setPassType(() => (passType === 'password' ? 'text' : 'password'));
  const changeConfirmType = () =>
    setConfirmType(() => (confirmType === 'password' ? 'text' : 'password'));

  const onCloseError = () => dispatch(removeRegisterError());

  useEffect(() => {
    dispatch(removeRegisterError());
    return () => dispatch(removeRegisterError());
  }, []);

  useEffect(() => {
    if (authToken) navigate('/');
  }, [authToken]);

  return (
    <AuthFormContainer>
      <div className={style.head}>
        <h1>Register</h1>
        <p>Please fill in all fields to create your account</p>
      </div>

      <form onSubmit={handleSubmit(submit)}>
        <div className={style.block}>
          <Controller
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <CustomizeInput
                size="md"
                fullWidth={true}
                value={value}
                onChange={onChange}
                error={error}
                label="Email"
                placeholder="Ex: brad@gmail.com"
                type="text"
              />
            )}
            name="email"
            control={control}
          />
        </div>
        <div className={style.block}>
          <Controller
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <CustomizeInput
                size="md"
                fullWidth={true}
                value={value}
                onChange={onChange}
                error={error}
                label="Password"
                placeholder="Password"
                type={passType}
                endAdornment={
                  <IconButton onClick={changePassType} size="small">
                    {passType === 'password' ? <VisibilityOutlined /> : <VisibilityOffOutlined />}
                  </IconButton>
                }
              />
            )}
            name="password"
            control={control}
          />
        </div>
        <div className={style.block}>
          <Controller
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <CustomizeInput
                size="md"
                fullWidth={true}
                value={value}
                onChange={onChange}
                error={error}
                label="Password confirmation"
                placeholder="Password confirmation"
                type={confirmType}
                endAdornment={
                  <IconButton onClick={changeConfirmType} size="small">
                    {confirmType === 'password' ? (
                      <VisibilityOutlined />
                    ) : (
                      <VisibilityOffOutlined />
                    )}
                  </IconButton>
                }
              />
            )}
            name="password_confirm"
            control={control}
          />
        </div>
        <div className={`${style.block} ${style.left}`}>
          <small>
            Already have an account? <Link to="/login">Login</Link>
          </small>
        </div>
        {error && (
          <div className={style.block}>
            <Alert onClose={onCloseError} title="Errro" severity="error">
              {error}
            </Alert>
          </div>
        )}
        <div className={style.block}>
          <CustomizeButton
            disabled={!isDirty || !isValid || isLoading}
            fullWidth={true}
            size="large"
            title="Register"
            type="submit"
            variant="contained"
            color="primary"
            loading={isLoading}
          />
        </div>
      </form>
    </AuthFormContainer>
  );
};

export default Register;
