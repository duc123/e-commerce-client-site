import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import CustomizeButton from '../../../components/CustomizeButton';
import { setPayment } from '../../../redux/cartSlice';
import style from './style.module.scss';

const Cash = () => {
  return <div>Cash</div>;
};

const Paypal = () => {
  return <div>Paypal</div>;
};

const Bank = () => {
  return <div>Bank</div>;
};

const Payment = () => {
  const { shipping, payment } = useSelector((state) => state.cartReducer);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!shipping.city) navigate('/checkout');
  }, [shipping]);

  const options = ['Bank', 'Paypal', 'Cash on delivery'];

  const renderMethod = () => {
    switch (payment) {
      case 'Paypal':
        return <Paypal />;
      case 'Bank':
        return <Bank />;
      default:
        return <Cash />;
    }
  };

  const handleChange = (e) => dispatch(setPayment(e.target.value));

  return (
    <div className={style.container}>
      <div className={style.option}>
        <FormControl className={style.form}>
          <RadioGroup
            aria-labelledby="demo-radio-buttons-group-label"
            value={payment}
            onChange={handleChange}
            name="radio-buttons-group">
            {options.map((el) => (
              <div className={style.block} key={el}>
                <FormControlLabel value={el} control={<Radio />} label={el} />
              </div>
            ))}
          </RadioGroup>
        </FormControl>
      </div>
      <div className={style.method}>{renderMethod()}</div>
    </div>
  );
};

export default Payment;
