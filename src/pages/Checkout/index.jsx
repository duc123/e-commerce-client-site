import React from 'react';
import { useSelector } from 'react-redux';
import { Link, Outlet } from 'react-router-dom';
import CheckoutInfo from './CheckoutInfo';
import CheckoutSteps from './CheckoutSteps';
import style from './style.module.scss';
import EmptyImg from '../../assets/images/cart/empty.png';
import CustomizeButton from '../../components/CustomizeButton';

const Empty = () => {
  return (
    <div className={style.empty}>
      <img src={EmptyImg} />
      <div>
        <p>Your cart is empty</p>
        <Link to="/">
          <CustomizeButton title="Back to home" variant="contained" size="lg" />
        </Link>
      </div>
    </div>
  );
};

const Checkout = () => {
  const { cartList } = useSelector((state) => state.cartReducer);

  return (
    <div className={style.container}>
      <div className={`wrapContainer`}>
        {cartList.length > 0 ? (
          <>
            <div className={style.main}>
              <div>
                <div className={style.left}>
                  <Outlet />
                </div>
              </div>
              <div>
                <CheckoutInfo />
              </div>
            </div>
          </>
        ) : (
          <Empty />
        )}
      </div>
    </div>
  );
};

export default Checkout;
