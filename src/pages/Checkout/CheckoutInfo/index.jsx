import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useMatch, useNavigate, useResolvedPath } from 'react-router-dom';
import CustomizeButton from '../../../components/CustomizeButton';
import CustomizeModal from '../../../components/CustomizeModal';
import { clearCart } from '../../../redux/cartSlice';
import { addNoti } from '../../../redux/notiSlice/slice';
import { addNewOrder } from '../../../utils/order';
import { calDeliveryFee, calTemp, calTotalTax, formatCurrency } from '../../../utils/price';
import style from './style.module.scss';

const Order = ({ cartList }) => {
  const getVariant = (item) => {
    const { variant, attributes } = item;
    return attributes.map((el) => el.key).map((el) => variant[el]);
  };

  return (
    <div className={style.order}>
      <div className={style.orderHead}>
        <h3>Your order</h3>
        <Link to="/carts">
          <CustomizeButton title="Change" size="xs" variant="outlined" type="button" />
        </Link>
      </div>
      {cartList.map((el, i) => (
        <div className={style.block} key={i}>
          <div className={style.blockLeft}>
            <span>{el.quantity}</span>
            <span>x</span>
            <span>
              {el.title} {el.productType === 'complex' && `(${getVariant(el)})`}
            </span>
          </div>
          <p>{el.price * el.quantity}$</p>
        </div>
      ))}
    </div>
  );
};

const ShippingAddress = () => {
  const { shipping } = useSelector((state) => state.cartReducer);

  const { city, district, ward, detail, phone } = shipping;

  return (
    <div className={style.container}>
      <div className={style.orderHead}>
        <h3>Shipping to</h3>
        <Link to="/checkout/shipping">
          <CustomizeButton title="Change" size="xs" />
        </Link>
      </div>
      <p>{phone}</p>
      <p>
        {city} / {district} / {ward}
      </p>
      <p>{detail}</p>
    </div>
  );
};

const CheckoutInfo = () => {
  const { cartList, shipping, payment } = useSelector((state) => state.cartReducer);

  const getTotal = () => calTemp(cartList) + calTotalTax(cartList) + calDeliveryFee();

  const path = useResolvedPath(`/checkout/payment`);
  const match = useMatch({ path: path.pathname, end: true });

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [loading, setLoading] = useState(false);

  const [modal, setModal] = useState(false);

  const handleClose = () => {
    dispatch(clearCart());
    setModal(false);
    navigate('/account/orders');
  };

  const content = () => {
    return (
      <div style={{ textAlign: 'center' }}>
        <h3 style={{ marginBottom: '10px' }}>Order successfully</h3>
        <p style={{ marginBottom: '10px' }}>Thank you for your purchases</p>
        <CustomizeButton
          onClick={handleClose}
          color="success"
          title="Ok"
          variant="contained"
          size="medium"
        />
      </div>
    );
  };

  const onConfirm = async () => {
    if (payment !== 'Cash on delivery') {
      dispatch(addNoti('Our company only currently accept cash payment method'));
      return '';
    }
    const orders = cartList.map((el) => ({ ...el, paymentMethod: payment }));

    setLoading(true);

    try {
      const res = await addNewOrder({ orders, shipping });
      console.log(res);
      setModal(true);
    } catch (err) {
      dispatch(addNoti(err));
    }
    setLoading(false);
  };

  return (
    <>
      <div className={style.container}>
        <CustomizeModal open={modal} handleClose={handleClose} content={content} noFooter />
        <Order cartList={cartList} />
        <div className={style.detailPrice}>
          <div className={style.priceItem}>
            <p>Subtotal:</p>
            <p>{formatCurrency(calTemp(cartList))}</p>
          </div>
          <div className={style.priceItem}>
            <p>Tax:</p>
            <p>{calTotalTax(cartList)}</p>
          </div>
          <div className={style.priceItem}>
            <p>Shipping:</p>
            <p>{calDeliveryFee()}</p>
          </div>
          <div className={style.priceItem}>
            <p>Total:</p>
            <p>{formatCurrency(getTotal())} $</p>
          </div>
        </div>
      </div>
      {match && (
        <>
          <ShippingAddress />
          <CustomizeButton
            loading={loading}
            disabled={loading}
            onClick={onConfirm}
            fullWidth
            title="Confirm"
            variant="contained"
            size="large"
            color="primary"
          />
        </>
      )}
    </>
  );
};

export default CheckoutInfo;
