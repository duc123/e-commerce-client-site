import React, { useEffect, useState } from 'react';
import style from './style.module.scss';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { getCities, getDistricts, getWards } from '../../../utils/getProvince';
import CustomizeInput from '../../../components/CustomizeInput';
import CustomizeSelect from '../../../components/CustomizeSelect';
import CustomizeButton from '../../../components/CustomizeButton';
import { setShipping } from '../../../redux/cartSlice';
import { useNavigate } from 'react-router-dom';

const Shipping = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const schema = yup.object().shape({
    city: yup.string().required(),
    district: yup.string().required(),
    ward: yup.string().required(),
    detail: yup.string().required(),
    phone: yup
      .string()
      .required()
      .matches(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/, 'Invalid phone number')
  });

  const { handleSubmit, control, watch, setValue } = useForm({
    resolver: yupResolver(schema)
  });

  const [cities, setCities] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);

  const watchCity = watch('city');
  const watchDistrict = watch('district');

  const [wardCount, setWardCount] = useState(0);
  const [districtCount, setDistrictCount] = useState(0);

  const getCitiesNames = () => (cities.length > 0 ? cities.map((el) => el['province_name']) : []);
  const getDistrictsNames = () =>
    districts.length > 0 ? districts.map((el) => el['district_name']) : [];

  const getWardsNames = () => (wards.length > 0 ? wards.map((el) => el['ward_name']) : []);

  const getCityId = () =>
    cities.length > 0
      ? cities.find((el) => el['province_name'] === watch('city'))?.province_id
      : '';

  const getDistrictId = () =>
    districts.length > 0
      ? districts.find((el) => el['district_name'] === watch('district'))?.district_id
      : '';

  const { client, isLoading } = useSelector((state) => state.clientReducer);
  const { shipping } = useSelector((state) => state.cartReducer);

  const submit = (data) => {
    dispatch(setShipping(data));
    navigate('/checkout/payment');
  };

  useEffect(() => {
    if (client) {
      if (!shipping.city) dispatch(setShipping(client.address));
    }
  }, [client]);

  // Fetch cities list
  useEffect(() => {
    getCities().then((data) => setCities(data.results));
  }, []);

  // Fetch district list
  useEffect(() => {
    if (watchCity) {
      const cityId = getCityId();
      getDistricts(cityId).then((data) => setDistricts(data.results));
    } else {
      setDistricts([]);
    }
  }, [watchCity]);

  // Fetch ward list
  useEffect(() => {
    if (watchDistrict) {
      const districtId = getDistrictId();
      getWards(districtId).then((data) => setWards(data.results));
    } else {
      setWards([]);
    }
  }, [watchDistrict]);

  useEffect(() => {
    if (shipping) {
      const { city } = shipping;
      if (cities.length > 0 && city) setValue('city', city);
    }
  }, [shipping, cities]);

  // Rest district when city changes

  useEffect(() => {
    setValue('district', '');
  }, [watchCity]);

  // Set values initially
  useEffect(() => {
    if (shipping) {
      const { phone, detail } = shipping;
      setValue('phone', phone);
      setValue('detail', detail);
    }
  }, [shipping]);

  // Set district initially

  useEffect(() => {
    if (shipping) {
      const { district } = shipping;
      // Only set district on time after shipping is defined
      if (districts.length > 0 && districtCount <= 0) {
        setValue('district', district);
        setDistrictCount(districtCount + 1);
      }
    }
  }, [shipping, districts]);

  //Set ward initially
  useEffect(() => {
    if (shipping) {
      const { ward } = shipping;
      // Only set ward on time after shipping is defined
      if (wards.length > 0 && wardCount <= 0) {
        setValue('ward', ward);
        setWardCount(wardCount + 1);
      }
    }
  }, [shipping, wards]);

  // Reset ward when district changes
  useEffect(() => {
    setValue('ward', '');
  }, [watchDistrict]);

  return (
    <div>
      <div className={style.container}>
        <form onSubmit={handleSubmit(submit)}>
          <div className={style.block}>
            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth={true}
                  value={value}
                  onChange={onChange}
                  error={error}
                  type="text"
                  size="sm"
                  label="Phone number"
                />
              )}
              name="phone"
              control={control}
            />
          </div>
          {/* City */}
          <div className={style.block}>
            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => {
                return (
                  <CustomizeSelect
                    size="md"
                    value={value}
                    onChange={onChange}
                    error={error}
                    options={getCitiesNames()}
                    disabled={cities.length <= 0 ? true : false}
                    placeholder="Chose a city"
                    label="City"
                  />
                );
              }}
              name="city"
              control={control}
            />
          </div>
          {/* District */}
          <div className={style.block}>
            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => {
                return (
                  <CustomizeSelect
                    size="md"
                    value={value}
                    onChange={onChange}
                    error={error}
                    options={getDistrictsNames()}
                    disabled={districts.length <= 0 ? true : false}
                    placeholder="Chose a district"
                    label="District"
                  />
                );
              }}
              name="district"
              control={control}
            />
          </div>
          {/* District */}
          <div className={style.block}>
            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => {
                return (
                  <CustomizeSelect
                    size="md"
                    value={value}
                    onChange={onChange}
                    error={error}
                    options={getWardsNames()}
                    disabled={districts.length <= 0 ? true : false}
                    placeholder="Chose a ward"
                    label="Ward"
                  />
                );
              }}
              name="ward"
              control={control}
            />
          </div>
          <div className={style.block}>
            <Controller
              render={({ field: { onChange, value }, fieldState: { error } }) => (
                <CustomizeInput
                  fullWidth={true}
                  value={value}
                  onChange={onChange}
                  error={error}
                  type="text"
                  size="sm"
                  multiline={true}
                  rows={4}
                  label="Address detail"
                />
              )}
              name="detail"
              control={control}
            />
          </div>
          <div className={style.btnBlock}>
            <CustomizeButton
              loading={isLoading}
              type="submit"
              title="To payment"
              color="primary"
              variant="contained"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default Shipping;
