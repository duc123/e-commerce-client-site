import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getProduct } from '../../api/product';
import ComplexProduct from '../../components/ComplexProduct';
import CustomizeButton from '../../components/CustomizeButton';
import Reviews from '../../components/Reviews';
import SimpleProduct from '../../components/SimpleProduct';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import axiosClient from '../../libs/axiosClient';
import { addNoti } from '../../redux/notiSlice/slice';
import { Alert } from '@mui/material';
import Loading from '../../components/Loading';

const Description = ({ description }) => {
  return <div>{description}</div>;
};

Description.propTypes = {
  description: PropTypes.string
};

const Product = () => {
  const params = useParams();

  const blocks = ['description', 'reviews'];

  const [product, setProduct] = useState(null);
  const [currentBlock, setCurrentBlock] = useState(blocks[0]);

  const [loading, setLoading] = useState(false);

  const [reviews, setReviews] = useState([]);

  const dispatch = useDispatch();

  const fetchReviews = async () => {
    try {
      const res = await axiosClient({
        url: `/reviews/${product._id}`,
        method: 'GET'
      });
      setReviews(res.data.reviews);
    } catch (err) {
      console.log(err);
    }
  };

  const addNewReview = (review) => {
    setReviews([...reviews, review]);
  };

  useEffect(() => {
    if (product) fetchReviews();
  }, [product]);

  useEffect(() => {
    window.scroll(0, 0);
  }, []);

  const { id } = params;

  const fetchProduct = async () => {
    setLoading(true);
    try {
      const product = await getProduct(id);
      setProduct(product);
    } catch (err) {
      dispatch(addNoti(err));
    }
    setLoading(false);
  };

  useEffect(() => {
    if (id) fetchProduct();
  }, [id]);

  const changeBlock = (bl) => () => {
    setCurrentBlock(bl);
  };

  const renderBlock = () => {
    switch (currentBlock) {
      case 'description':
        return <Description description={product.description} />;
      default:
        return <Reviews productId={product._id} addNewReview={addNewReview} reviews={reviews} />;
    }
  };

  return loading ? (
    <div className={style.center}>
      <Loading />
    </div>
  ) : (
    <div className={`wrapContainer ${style.container}`}>
      {product ? (
        <>
          {product?.productType === 'simple' ? (
            <SimpleProduct product={product} />
          ) : (
            <ComplexProduct product={product} />
          )}
          <div className={style.blocks}>
            {blocks.map((bl) => (
              <p
                onClick={changeBlock(bl)}
                key={bl}
                className={`${style.block} ${bl === currentBlock && style.active}`}>
                <CustomizeButton
                  title={`${bl} ${bl === 'reviews' ? ` (${reviews?.length})` : ''}`}
                  size="medium"
                  color="neutral"
                  className={style.block}
                />
              </p>
            ))}
          </div>
          <div className={style.blockPart}>{renderBlock()}</div>
        </>
      ) : (
        <div className={style.center}>
          <Alert sx={{ width: '100%' }} variant="outlined" severity="info">
            No products
          </Alert>{' '}
        </div>
      )}
    </div>
  );
};

export default Product;
