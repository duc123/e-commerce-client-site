import { Alert, Checkbox, FormControlLabel, FormGroup, Pagination } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { getByCondition } from '../../api/product';
import Loading from '../../components/Loading';
import ProductList from '../../components/ProductList';
import { addNoti } from '../../redux/notiSlice/slice';
import style from './style.module.scss';

const ProductPagination = ({ count, page, onChange }) => {
  return (
    <div style={{ marginTop: '15px' }}>
      <Pagination count={count} page={page} onChange={onChange} color="primary" />
    </div>
  );
};

const ProductsByCondition = () => {
  const [products, setProducts] = useState([]);

  const [pageCount, setPageCount] = useState(1);

  const [total, setTotal] = useState(0);

  const [searchParams] = useSearchParams();

  const [loading, setLoading] = useState(false);

  const sortArr = [
    {
      label: 'High to low',
      value: 'high'
    },
    {
      label: 'Low to high',
      value: 'low'
    },
    {
      label: 'Default',
      value: ''
    }
  ];

  const subCategory = searchParams.get('subC');
  const category = searchParams.get('c');
  const page = searchParams.get('p') > 1 ? +searchParams.get('p') : 1;
  const sort = searchParams.get('pSort') ? searchParams.get('pSort') : '';

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const getProducts = async () => {
    const query = { subCategory, category, page, pSort: sort };
    setLoading(true);
    try {
      const data = await getByCondition(query);
      const { pageCount: pCount, total: pTotal, products: list } = data;
      setPageCount(pCount);
      setTotal(pTotal);
      setProducts(list);
    } catch (err) {
      dispatch(addNoti(err));
    }
    setLoading(false);
  };

  useEffect(() => {
    getProducts();
    window.scrollTo(0, 0);
  }, [subCategory, category, page, sort]);

  const handlePageChange = (e, num) => {
    navigate(
      `/products/condition?subC=${subCategory}&&c=${
        category ? category : ''
      }&&p=${num}&&pSort=${sort}`
    );
  };

  const onSort = (val) => () => {
    navigate(`/products/condition?subC=${subCategory}&&c=${category}&&p=${page}&&pSort=${val}`);
  };

  return (
    <div className={style.wrap}>
      <div className="wrapContainer">
        <div className={style.container}>
          <div className={style.left}>
            <div>
              <h4>Sort price</h4>

              {sortArr.map((el, i) => (
                <FormControlLabel
                  key={i}
                  onChange={onSort(el.value)}
                  checked={sort === el.value}
                  control={<Checkbox />}
                  label={el.label}
                  value={el.value}
                />
              ))}
            </div>
          </div>
          <div>
            {loading ? (
              <div className={style.center}>
                <Loading />
              </div>
            ) : products.length > 0 ? (
              <>
                <ProductList sm list={products} />
                <ProductPagination count={pageCount} page={page} onChange={handlePageChange} />
              </>
            ) : (
              <div className={style.center}>
                <Alert sx={{ width: '100%' }} variant="outlined" severity="info">
                  No products
                </Alert>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductsByCondition;
