import React, { useEffect, useState } from 'react';
import AuthFormContainer from '../../components/AuthFormContainer';
import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import CustomizeInput from '../../components/CustomizeInput';
import style from './style.module.scss';
import { VisibilityOffOutlined, VisibilityOutlined } from '@mui/icons-material';
import { Alert, IconButton } from '@mui/material';
import CustomizeButton from '../../components/CustomizeButton';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { removeLoginError } from '../../redux/loginSlice';
import { loginRequest } from '../../redux/loginSlice/api';

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { isLoading, error, authToken } = useSelector((state) => state.loginReducer);

  const schema = yup.object().shape({
    email: yup.string().required('Email can not be blank').email('Invalid email'),
    password: yup.string().required('Password can not be blank').min(6)
  });

  const {
    handleSubmit,
    control,
    formState: { isDirty, isValid }
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'all'
  });

  const submit = (data) => {
    dispatch(loginRequest(data));
  };

  const [passType, setPassType] = useState('password');

  const changePassType = () => setPassType(() => (passType === 'password' ? 'text' : 'password'));

  const onCloseError = () => dispatch(removeLoginError());

  useEffect(() => {
    dispatch(removeLoginError());
    return () => dispatch(removeLoginError());
  }, []);

  useEffect(() => {
    if (authToken) navigate(-1);
  }, [authToken]);

  return (
    <AuthFormContainer>
      <div className={style.head}>
        <h1>Login</h1>
        <p>Sign in with your email and password</p>
      </div>

      <form onSubmit={handleSubmit(submit)}>
        <div className={style.block}>
          <Controller
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <CustomizeInput
                size="md"
                fullWidth={true}
                value={value}
                onChange={onChange}
                error={error}
                label="Email"
                placeholder="Ex: brad@gmail.com"
                type="text"
              />
            )}
            name="email"
            control={control}
          />
        </div>
        <div className={style.block}>
          <Controller
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <CustomizeInput
                size="md"
                fullWidth={true}
                value={value}
                onChange={onChange}
                error={error}
                label="Password"
                placeholder="Password"
                type={passType}
                endAdornment={
                  <IconButton onClick={changePassType} size="small">
                    {passType === 'password' ? <VisibilityOutlined /> : <VisibilityOffOutlined />}
                  </IconButton>
                }
              />
            )}
            name="password"
            control={control}
          />
        </div>
        <div className={`${style.block} ${style.left}`}>
          <small>
            Do not have an account? <Link to="/register">Register</Link>
          </small>
        </div>
        {error && (
          <div className={style.block}>
            <Alert onClose={onCloseError} title="Errro" severity="error">
              {error}
            </Alert>
          </div>
        )}
        <div className={style.block}>
          <CustomizeButton
            disabled={!isDirty || !isValid || isLoading}
            fullWidth={true}
            size="large"
            title="Login"
            type="submit"
            variant="contained"
            color="primary"
            loading={isLoading}
          />
        </div>
        <div className={style.right}>
          <small>
            Forget password? <Link to="/reset-password">Click here</Link>
          </small>
        </div>
      </form>
    </AuthFormContainer>
  );
};

export default Login;
