import { ArrowRightAltOutlined } from '@mui/icons-material';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import CartItem from '../../components/CartItem';
import CustomizeButton from '../../components/CustomizeButton';
import { calDeliveryFee, calTemp, calTotalTax, formatCurrency } from '../../utils/price';
import style from './style.module.scss';

const Empty = () => {
  return <div></div>;
};

const CartList = ({ cartList }) => {
  return (
    <div>
      <div className={style.list}>
        <div>
          {cartList.map((item, idx) => (
            <CartItem key={idx} item={item} />
          ))}
        </div>
      </div>
    </div>
  );
};

const CartPage = () => {
  const { cartList } = useSelector((state) => state.cartReducer);

  const getTotal = () => {
    return calTemp(cartList) + calTotalTax(cartList) + calDeliveryFee();
  };
  useEffect(() => {
    window.scroll(0, 0);
  }, []);

  return (
    <div className={style.container}>
      <div className={`${style.grid} wrapContainer`}>
        {cartList.length > 0 ? (
          <>
            <CartList cartList={cartList} />
            <div>
              <div className={style.checkout}>
                <div className={style.block}>
                  <p>Temporary</p>
                  <span>{formatCurrency(calTemp(cartList))} $</span>
                </div>
                <div className={style.block}>
                  <p>Tax:</p>
                  <span>{formatCurrency(calTotalTax(cartList))} $</span>
                </div>
                <div className={style.block}>
                  <p>Shipping:</p>
                  <span>{calDeliveryFee()} $</span>
                </div>
                <div className={style.block}>
                  <p></p>
                  <p>{formatCurrency(getTotal())} $</p>
                </div>
                <Link to="/checkout">
                  <CustomizeButton
                    endIcon={<ArrowRightAltOutlined />}
                    title="Checkout now"
                    fullWidth
                    variant="contained"
                    color="primary"
                  />
                </Link>
              </div>
            </div>
          </>
        ) : (
          <Empty />
        )}
      </div>
    </div>
  );
};

export default CartPage;
