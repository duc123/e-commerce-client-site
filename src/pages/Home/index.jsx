import React from 'react';
import Advantages from '../../components/Advantages';
import CarouselHero from '../../components/CarouselHero';
import RandomProducts from '../../components/RandomProducts';
import ShowCase from '../../components/Showcase';
const Home = () => {
  return (
    <div>
      <CarouselHero />
      <div className={`wrapContainer`}>
        <ShowCase />
        <RandomProducts />
        <Advantages />
      </div>
    </div>
  );
};

export default Home;
