import React from 'react';
import { Link } from 'react-router-dom';
import AuthFormContainer from '../../components/AuthFormContainer';
import CustomizeButton from '../../components/CustomizeButton';
import style from './style.module.scss';

const ResetPassword = () => {
  return (
    <AuthFormContainer>
      <div className={style.container}>
        <h2>Reset password</h2>
        <p>Reset password function is not ready yet.</p>
        <div className={style.flex}>
          <Link to="/login">
            <CustomizeButton title="To login" variant="outlined" size="large" />
          </Link>
          <Link to="/">
            <CustomizeButton title="Back to home" variant="contained" size="large" />
          </Link>
        </div>
      </div>
    </AuthFormContainer>
  );
};

export default ResetPassword;
