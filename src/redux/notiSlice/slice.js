import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  message: ''
};

const notiSlice = createSlice({
  name: 'notiSlice',
  initialState,
  reducers: {
    addNoti: (state, action) => {
      state.message = action.payload;
    },
    removeNoti: (state) => {
      state.message = '';
    }
  }
});

const { reducer: notiReducer } = notiSlice;

export const {
  actions: { addNoti, removeNoti }
} = notiSlice;

export default notiReducer;
