import { configureStore } from '@reduxjs/toolkit';
import loginReducer from './loginSlice';
import registerReducer from './registerSlice';
import categoryReducer from './categorySlice';
import clientReducer from './clientSlice';
import cartReducer from './cartSlice';
import notiReducer from './notiSlice/slice';
import randomProductReducer from './randomProductSlice';

export const store = configureStore({
  reducer: {
    loginReducer,
    registerReducer,
    categoryReducer,
    clientReducer,
    cartReducer,
    notiReducer,
    randomProductReducer
  }
});
