import { createSlice } from '@reduxjs/toolkit';
import { getCategories } from './api';

const initialState = {
  categories: [],
  isLoading: false
};

const categorySlice = createSlice({
  name: 'categoryReducer',
  initialState,
  extraReducers: (builder) => {
    builder.addCase(getCategories.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(getCategories.fulfilled, (state, action) => {
      state.isLoading = false;
      const {
        data: { categories }
      } = action.payload;
      state.categories = categories;
    });
    builder.addCase(getCategories.rejected, (state) => {
      state.isLoading = false;
    });
  }
});

const { reducer: categoryReducer } = categorySlice;

export default categoryReducer;
