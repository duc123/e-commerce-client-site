import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../libs/axiosClient';
import { addNoti } from '../notiSlice/slice';

export const getCategories = createAsyncThunk('getCategories', async (data, dispatchApi) => {
  try {
    const res = await axiosClient({
      method: 'GET',
      url: '/categories'
    });
    return res;
  } catch (err) {
    dispatchApi.dispatch(addNoti(err.response.data.error));
  }
});
