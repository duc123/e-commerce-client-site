import { createSlice } from '@reduxjs/toolkit';
import { getRandomProducts } from './api';

const initialState = {
  randomProducts: [],
  loading: false,
  current: 1,
  limit: 1,
  total: 1
};

const randomProductSlice = createSlice({
  name: 'randomProductSlice',
  initialState,
  reducers: {
    setCurrent: (state, action) => {
      state.current = action.payload;
    }
  },
  extraReducers: (builder) => {
    builder.addCase(getRandomProducts.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getRandomProducts.fulfilled, (state, action) => {
      state.loading = false;
      const { products, pageCount, total } = action.payload.data;
      if (state.current === 1) {
        state.randomProducts = products;
      } else {
        state.randomProducts = [...state.randomProducts, ...products];
      }
      state.limit = pageCount;
      state.total = total;
    });
    builder.addCase(getRandomProducts.rejected, (state) => {
      state.loading = false;
    });
  }
});

const { reducer: randomProductReducer } = randomProductSlice;

export const {
  actions: { setCurrent }
} = randomProductSlice;

export default randomProductReducer;
