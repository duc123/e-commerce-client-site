import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../libs/axiosClient';
import { addNoti } from '../notiSlice/slice';

export const getRandomProducts = createAsyncThunk(
  'getRandomProducts',
  async (current, dispatch) => {
    try {
      const res = await axiosClient({
        url: `/products/getRandom/?page=${current}`,
        method: 'GET'
      });
      return res;
    } catch (err) {
      dispatch.dispatch(addNoti(err.response.data.error));
    }
  }
);
