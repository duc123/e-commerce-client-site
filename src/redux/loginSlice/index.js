import { createSlice } from '@reduxjs/toolkit';
import { loginRequest, logoutRequest } from './api';

const authToken = JSON.stringify(localStorage.getItem('authToken'));

const initialState = {
  authToken: authToken ? JSON.parse(authToken) : '',
  error: '',
  isLoading: false
};

const saveJwt = (token) => {
  localStorage.setItem('authToken', JSON.stringify(token));
};

const removeJwt = () => {
  localStorage.removeItem('authToken');
};

const loginSlice = createSlice({
  name: 'loginSlice',
  initialState,
  reducers: {
    removeLoginError: (state) => {
      state.error = '';
    },
    onRegisterSucceed: (state, action) => {
      state.authToken = action.payload;
      saveJwt(action.payload);
    },
    forceLogout: (state) => {
      state.authToken = '';
      removeJwt();
    }
  },
  extraReducers: (builder) => {
    // LOGIN
    builder.addCase(loginRequest.pending, (state) => {
      state.error = '';
      state.isLoading = true;
    });
    builder.addCase(loginRequest.fulfilled, (state, action) => {
      state.error = '';
      state.isLoading = false;
      const {
        data: { token }
      } = action.payload;
      state.authToken = token;
      saveJwt(token);
    });
    builder.addCase(loginRequest.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    });

    // LOGOUT
    builder.addCase(logoutRequest.pending, (state) => {
      state.error = '';
      state.isLoading = true;
    });
    builder.addCase(logoutRequest.fulfilled, (state) => {
      state.error = '';
      state.isLoading = false;
      state.authToken = '';
      removeJwt();
    });
    builder.addCase(logoutRequest.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    });
  }
});

const { reducer: loginReducer } = loginSlice;

export const {
  actions: { removeLoginError, onRegisterSucceed, forceLogout }
} = loginSlice;

export default loginReducer;
