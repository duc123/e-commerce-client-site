import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../libs/axiosClient';
import { clearCart } from '../cartSlice';

export const loginRequest = createAsyncThunk('loginRequest', async (data) => {
  try {
    const res = await axiosClient({
      url: '/auth/user/login',
      method: 'POST',
      data
    });
    return res;
  } catch (err) {
    throw err.response.data.error;
  }
});

export const logoutRequest = createAsyncThunk('logoutRequest', async (data, dispatchApi) => {
  try {
    const res = await axiosClient({
      url: '/auth/user/logout',
      method: 'POST'
    });
    dispatchApi.dispatch(clearCart());
    return res;
  } catch (err) {
    throw err.response.data.error;
  }
});
