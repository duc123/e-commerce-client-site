import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../libs/axiosClient';

export const registerRequest = createAsyncThunk('registerRequest', async (data) => {
  try {
    const res = await axiosClient({
      url: '/auth/user/register',
      method: 'POST',
      data
    });
    return res;
  } catch (err) {
    throw err.response.data.error;
  }
});
