import { createSlice } from '@reduxjs/toolkit';
import { registerRequest } from './api';

const initialState = {
  error: '',
  isLoading: false
};

const registerSlice = createSlice({
  name: 'registerSlice',
  initialState,
  reducers: {
    removeRegisterError: (state) => {
      state.error = '';
    }
  },
  extraReducers: (builder) => {
    // LOGIN
    builder.addCase(registerRequest.pending, (state) => {
      state.error = '';
      state.isLoading = true;
    });
    builder.addCase(registerRequest.fulfilled, (state) => {
      state.error = '';
      state.isLoading = false;
    });
    builder.addCase(registerRequest.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error.message;
    });
  }
});

const { reducer: registerReducer } = registerSlice;

export const {
  actions: { removeRegisterError }
} = registerSlice;

export default registerReducer;
