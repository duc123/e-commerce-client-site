import { createAsyncThunk } from '@reduxjs/toolkit';
import axiosClient from '../../libs/axiosClient';
import { addNoti } from '../notiSlice/slice';

export const getClient = createAsyncThunk('getClient', async (data, dispatchApi) => {
  try {
    const res = await axiosClient({
      url: '/clients/getInfo',
      method: 'GET'
    });
    return res;
  } catch (err) {
    dispatchApi.dispatch(addNoti(err.response.data.error));
    throw err.response.data.error;
  }
});

export const updateClientGeneralInfo = createAsyncThunk(
  'updateClientGeneralInfo',
  async (data, dispatchApi) => {
    try {
      const res = await axiosClient({
        url: '/clients/updateGeneralInfo',
        method: 'PUT',
        data: data
      });
      return res;
    } catch (err) {
      dispatchApi.dispatch(addNoti(err.response.data.error));
      throw err.response.data.error;
    }
  }
);

export const updateClientAddress = createAsyncThunk(
  'updateClientAddress',
  async (data, dispatchApi) => {
    try {
      const res = await axiosClient({
        url: '/clients/updateClientAddress',
        method: 'PUT',
        data: data
      });
      return res;
    } catch (err) {
      dispatchApi.dispatch(addNoti(err.response.data.error));
      throw err.response.data.error;
    }
  }
);
