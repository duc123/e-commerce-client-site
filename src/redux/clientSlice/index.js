import { createSlice } from '@reduxjs/toolkit';
import { getClient, updateClientAddress, updateClientGeneralInfo } from './api';

const initialState = {
  client: null,
  isLoading: false
};

const clientSlice = createSlice({
  name: 'clientSlice',
  initialState,
  extraReducers: (builder) => {
    // GET CLIENT
    builder.addCase(getClient.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(getClient.fulfilled, (state, action) => {
      state.isLoading = false;
      const {
        data: { client }
      } = action.payload;
      state.client = client;
    });
    builder.addCase(getClient.rejected, (state) => {
      state.isLoading = false;
    });
    // UPDATE CLIENT GENERAL INFO
    builder.addCase(updateClientGeneralInfo.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(updateClientGeneralInfo.fulfilled, (state, action) => {
      state.isLoading = false;
      const {
        data: { client }
      } = action.payload;
      state.client = client;
    });
    builder.addCase(updateClientGeneralInfo.rejected, (state) => {
      state.isLoading = false;
    });
    // UPDATE CLIENT ADDRESS
    builder.addCase(updateClientAddress.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(updateClientAddress.fulfilled, (state, action) => {
      state.isLoading = false;
      const {
        data: { client }
      } = action.payload;
      state.client = client;
    });
    builder.addCase(updateClientAddress.rejected, (state) => {
      state.isLoading = false;
    });
  }
});

const { reducer: clientReducer } = clientSlice;

export default clientReducer;
