import { createSlice } from '@reduxjs/toolkit';

const cartList = localStorage.getItem('cartList');

const initialState = {
  cartList: cartList ? JSON.parse(cartList) : [],
  shipping: {
    city: '',
    district: '',
    ward: '',
    detail: '',
    phone: ''
  },
  payment: 'Cash on delivery',
  cartVisible: false
};

const saveCartToLocal = (list) => {
  localStorage.setItem('cartList', JSON.stringify(list));
};

const addSimple = (product, state) => {
  const { title, price, _id: id, images, productType, stock } = product;
  if (productType === 'simple') {
    if (state.cartList.find((el) => el.productId === id)) {
      const productInfo = { id, productType, stock };

      updateSimple(productInfo, true, state);
    } else {
      const cartItem = {
        title,
        price,
        productId: id,
        quantity: 1,
        image: images[0],
        productType
      };
      state.cartList = [...state.cartList, cartItem];
      saveCartToLocal(state.cartList);
    }
  }
};

const addComplex = (product, variant, state) => {
  const { title, _id: id, images, productType, stock, attributes } = product;

  if (variant) {
    if (state.cartList.find((el) => el.productId === id && el.variant.id === variant.id)) {
      const productInfo = { id, productType };
      updateComplex(productInfo, true, variant, state);
    } else {
      const cartItem = {
        title,
        price: +variant.price,
        productId: id,
        quantity: 1,
        image: variant.images[0] ? variant.images[0] : images[0],
        productType,
        attributes,
        variant
      };
      state.cartList = [...state.cartList, cartItem];
      saveCartToLocal(state.cartList);
    }
  }
};

const updateSimple = (productInfo, isAdd, state) => {
  const { productType, id, stock } = productInfo;
  if (productType === 'simple') {
    const cartIndex = state.cartList.findIndex((el) => el.productId === id);
    if (cartIndex !== -1) {
      if (isAdd) {
        // console.log(stock);
        // if (state.cartList[cartIndex].quantity < +stock)
        state.cartList[cartIndex].quantity += 1;
      } else {
        if (state.cartList[cartIndex].quantity > 1) state.cartList[cartIndex].quantity -= 1;
      }
      saveCartToLocal(state.cartList);
    }
  }
};

const updateComplex = (productInfo, isAdd, variant, state) => {
  const { productType, id } = productInfo;

  const { id: variantId, stock } = variant;

  if (productType === 'complex') {
    const cartIndex = state.cartList.findIndex(
      (el) => el.productId === id && el.variant.id === variantId
    );
    if (cartIndex !== -1) {
      if (isAdd) {
        // if (state.cartList[cartIndex].quantity < stock)
        state.cartList[cartIndex].quantity += 1;
      } else {
        if (+state.cartList[cartIndex].quantity > 1) state.cartList[cartIndex].quantity -= 1;
      }
      saveCartToLocal(state.cartList);
    }
  }
};

const cartSlice = createSlice({
  name: 'cartSlice',
  initialState,
  reducers: {
    addToCart: (state, action) => {
      const { product, variant } = action.payload;
      const { productType } = product;
      if (productType === 'simple') {
        addSimple(product, state);
      } else {
        addComplex(product, variant, state);
      }
      state.cartVisible = true;
    },
    updateQuantity: (state, action) => {
      const { productInfo, isAdd, variant } = action.payload;
      const { productType, id, stock } = productInfo;
      if (productType === 'simple') {
        updateSimple({ id, productType, stock }, isAdd, state);
      } else {
        updateComplex({ id, productType }, isAdd, variant, state);
      }
    },
    removeCartItem: (state, action) => {
      const { productType, productId, variantId } = action.payload;
      if (productType === 'simple') {
        state.cartList = state.cartList.filter((el) => el.productId !== productId);
      } else {
        const newList = [...state.cartList];
        const index = newList.findIndex(
          (el) => el.productId === productId && el.variant?.id === variantId
        );
        newList.splice(index, 1);
        state.cartList = newList;
      }
      saveCartToLocal(state.cartList);
    },
    clearCart: (state) => {
      state.cartList = [];
      saveCartToLocal(state.cartList);
    },
    setShipping: (state, action) => {
      state.shipping = action.payload;
    },
    showCart: (state) => {
      state.cartVisible = true;
    },
    hideCart: (state) => {
      state.cartVisible = false;
    },
    setPayment: (state, action) => {
      state.payment = action.payload;
    }
  }
});

const { reducer: cartReducer } = cartSlice;

export const {
  actions: {
    addToCart,
    updateQuantity,
    removeCartItem,
    clearCart,
    setShipping,
    showCart,
    hideCart,
    setPayment
  }
} = cartSlice;

export default cartReducer;
