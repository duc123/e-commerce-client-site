import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { getSingleClient } from '../../api/user';
import style from './style.module.scss';
import {
  AccountBoxOutlined,
  AccountCircleOutlined,
  AccountTreeOutlined
} from '@mui/icons-material';
import { Rating } from '@mui/material';

const getTime = (val) => {
  const time = new Date(val);
  return `${time.getMonth()}/${time.getDate()}/${time.getFullYear()}`;
};

const MetaData = ({ item, user }) => {
  const { avatar, email } = user;
  const { rating, created_at } = item;

  return (
    <div className={style.meta}>
      {avatar ? (
        <img className={style.avatar} src={avatar} />
      ) : (
        <p className={style.iconContainer}>
          <AccountCircleOutlined className={style.icon} />
        </p>
      )}
      <div className={style.metaRight}>
        <p>{email}</p>
        <Rating className={style.rating} value={rating} readOnly size="small" />
        <small className={style.date}>{getTime(created_at)}</small>
      </div>
    </div>
  );
};

MetaData.propTypes = {
  item: PropTypes.object,
  user: PropTypes.object
};

const ReviewItem = ({ item }) => {
  const [user, setUser] = useState(null);

  const { uid, content } = item;

  const getUser = async () => {
    try {
      const client = await getSingleClient(uid);
      setUser(client);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    if (!user) getUser();
  }, [uid]);

  return (
    <div className={style.container}>
      {user && <MetaData item={item} user={user} />}
      <div>
        <p className={style.content}>{content}</p>
      </div>
    </div>
  );
};

ReviewItem.propTypes = {
  item: PropTypes.object
};

export default ReviewItem;
