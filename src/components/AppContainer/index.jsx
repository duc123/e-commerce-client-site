import React, { useEffect, useState } from 'react';
import Header from '../Header';
import TopBar from '../TopBar';
import PropTypes from 'prop-types';
import Footer from '../Footer';
import { useDispatch, useSelector } from 'react-redux';
import { getCategories } from '../../redux/categorySlice/api';
import style from './style.module.scss';
import { getClient } from '../../redux/clientSlice/api';

const AppContainer = ({ children }) => {
  const dispatch = useDispatch();

  const { authToken } = useSelector((state) => state.loginReducer);
  const { client } = useSelector((state) => state.clientReducer);

  useEffect(() => {
    let stop = false;
    if (!stop) dispatch(getCategories());
    return () => (stop = true);
  }, []);

  useEffect(() => {
    if (authToken) {
      if (!client) dispatch(getClient());
    }
  }, [authToken]);

  return (
    <div>
      <TopBar />
      <Header />
      <main className={style.main}>{children}</main>
      <Footer />
    </div>
  );
};

AppContainer.propTypes = {
  children: PropTypes.element
};

export default AppContainer;
