import React from 'react';
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
import style from './style.module.scss';
import PropTypes from 'prop-types';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

// import required modules
import { Autoplay, Pagination, Navigation } from 'swiper';
import CustomizeButton from '../CustomizeButton';
import { Link } from 'react-router-dom';

import Pharmacy from '../../assets/images/hero/pharmacy.webp';
import Vegetable from '../../assets/images/hero/vegetable.webp';
import Shoe from '../../assets/images/hero/nike-black.png';
import Jacket from '../../assets/images/hero/jacket.png';

// import Shoe from '../../assets/images/hero';

// import Jacket from '../../assets/images/hero';

const CarouselItem = ({ item }) => {
  const { title, img, url } = item;
  return (
    <div className={style.carouselItem}>
      <div className={style.container}>
        <div className={style.textContainer}>
          <h1>{title}</h1>
          <Link to={url}>
            <CustomizeButton title="Explore" color="primary" size="xl" variant="contained" />
          </Link>
        </div>
        <div className={style.imgContainer}>
          <img className={style.img} src={img} alt="" />
        </div>
      </div>
    </div>
  );
};

CarouselItem.propTypes = {
  item: PropTypes.object
};

export default function CarouselHero() {
  const items = [
    {
      title: 'Take care of your health',
      img: Pharmacy,
      url: '/products/condition?c=health'
    },
    {
      title: 'Comfortable shoes',
      img: Shoe,
      url: '/products/condition?c=shoes'
    },
    {
      title: 'Healthy food healthy life',
      img: Vegetable,
      url: '/products/condition?subC=vegetables'
    },
    {
      title: 'Stylish jacket',
      img: Jacket,
      url: '/products/condition?subC=jacket'
    }
  ];

  return (
    <>
      <Swiper
        loop={true}
        slidesPerView={1}
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 3500,
          disableOnInteraction: false
        }}
        pagination={{
          clickable: true
        }}
        navigation={true}
        modules={[Autoplay, Pagination, Navigation]}
        className={style.swiper}>
        {items.map((el, i) => (
          <SwiperSlide key={i}>
            <CarouselItem item={el} />
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
}
