import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import {
  LocalPhoneOutlined,
  LocalShippingOutlined,
  PaymentOutlined,
  PedalBikeOutlined
} from '@mui/icons-material';

const Item = ({ item }) => {
  const { key, title, content } = item;

  const getIcon = (name) => {
    switch (name) {
      case 'pickUp':
        return <PedalBikeOutlined />;
      case 'support':
        return <LocalPhoneOutlined />;
      case 'payment':
        return <PaymentOutlined />;
      default:
        return <LocalShippingOutlined />;
    }
  };

  return (
    <div className={style.item}>
      <span className={style.iconContainer}>{getIcon(key)}</span>
      <div className={style.textContainer}>
        <h3>{title}</h3>
        <p>{content}</p>
      </div>
    </div>
  );
};

Item.propTypes = {
  item: PropTypes.object
};

const Advantages = () => {
  const list = [
    {
      title: 'Fast delivery',
      content: 'Start from $10 centric approach',
      key: 'delivery'
    },
    {
      title: 'Pick up',
      content: 'Secure system centric approach',
      key: 'pickUp'
    },

    {
      title: 'Online support',
      content: '24/7 daily centric approach',
      key: 'support'
    },
    {
      title: 'Payment',
      content: 'Secure system',
      key: 'payment'
    }
  ];

  return (
    <div className={style.container}>
      {list.map((item) => (
        <Item item={item} key={item.key} />
      ))}
    </div>
  );
};

export default Advantages;
