import { EmailOutlined, PhoneOutlined } from '@mui/icons-material';
import React from 'react';
import style from './style.module.scss';

const TopBar = () => {
  return (
    <div className={`${style.topBar}`}>
      <div className="wrapContainer">
        <div className={style.container}>
          <div className={style.contact}>
            <p>
              <PhoneOutlined />
              <span>0383172117</span>
            </p>
            <p>
              <EmailOutlined />
              <span>luuminhduc00@gmail.com</span>
            </p>
          </div>

          <div className={style.help}>
            <p>Download app</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopBar;
