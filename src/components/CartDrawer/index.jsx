import { Drawer } from '@mui/material';
import React from 'react';
import Cart from '../Cart';

const CartDrawer = ({ cartVisible, hideCart }) => {
  return (
    <Drawer open={cartVisible} onClose={hideCart} anchor="right">
      <Cart />
    </Drawer>
  );
};

export default CartDrawer;
