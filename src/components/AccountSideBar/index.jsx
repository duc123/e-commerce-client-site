import React from 'react';
import Proptypes from 'prop-types';
import { Link, useMatch, useResolvedPath } from 'react-router-dom';
import style from './style.module.scss';
import { InfoOutlined } from '@mui/icons-material';

const CustomizeLink = ({ to }) => {
  const path = useResolvedPath(`${to}`);
  const match = useMatch({ path: path.pathname, end: true });

  const getIcon = () => {
    switch (to) {
      default:
        return <InfoOutlined />;
    }
  };

  return (
    <Link className={`${style.item} ${match && style.active}`} to={`${to}`}>
      {getIcon()}
      <span>{to}</span>
    </Link>
  );
};
CustomizeLink.propTypes = {
  to: Proptypes.string
};

const AccountSideBar = () => {
  const list = ['info', 'address', 'orders', 'notifications'];

  return (
    <div className={style.container}>
      {list.map((item) => (
        <CustomizeLink to={item} key={item} />
      ))}
    </div>
  );
};

export default AccountSideBar;
