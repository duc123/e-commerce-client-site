import React from 'react';
import { Link } from 'react-router-dom';
import style from './style.module.scss';

const AuthHeader = () => {
  return (
    <div className={style.container}>
      <Link className={style.title} to="/">
        <h1>Bestbuy</h1>
      </Link>
    </div>
  );
};

export default AuthHeader;
