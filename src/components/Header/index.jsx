import {
  ArrowDropDownOutlined,
  ArrowDropUpOutlined,
  ArrowRightAltOutlined,
  CloseOutlined,
  LogoutOutlined,
  MenuOutlined,
  ShoppingCartOutlined
} from '@mui/icons-material';
import { Badge, Divider, Drawer, IconButton, Popover } from '@mui/material';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import useScrollingUp from '../../hooks/useScrollingUp';
import { showCart } from '../../redux/cartSlice';
import { logoutRequest } from '../../redux/loginSlice/api';
import Cart from '../Cart';
import CustomizeButton from '../CustomizeButton';
import CustomizeModal from '../CustomizeModal';
import HeaderCategories from '../HeaderCatogories';
import SearchBar from '../SearchBar';
import style from './style.module.scss';

const AccountComponent = ({ setMobileNavShow }) => {
  const { authToken } = useSelector((state) => state.loginReducer);

  return authToken ? (
    <AccountNav setMobileNavShow={setMobileNavShow} />
  ) : (
    <Link to="/login">
      <CustomizeButton title="login" type="button" size="large" />
    </Link>
  );
};

const MobileNav = ({ open, onClose, setMobileNavShow }) => {
  return (
    <div className={style.onlySm}>
      <Drawer open={open} onClose={onClose}>
        <div className={style.mobileNav}>
          <div className={style.mobileNavHead}>
            <p></p>
            <IconButton onClick={onClose}>
              <CloseOutlined />
            </IconButton>
          </div>
          <div className={style.mobileItem}>
            <HeaderCategories />
          </div>
          <div className={style.mobileItem}>
            <AccountComponent setMobileNavShow={setMobileNavShow} />
          </div>
        </div>
      </Drawer>
    </div>
  );
};

const AccountNav = ({ setMobileNavShow }) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const [logoutVisible, setLogoutVisible] = useState(false);

  const open = Boolean(anchorEl);

  const dispatch = useDispatch();

  const handleOpen = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
    if (setMobileNavShow) setMobileNavShow(false);
  };

  const handleLogout = () => {
    setLogoutVisible(true);
  };

  const closeModal = () => {
    setLogoutVisible(false);
  };

  const callback = () => dispatch(logoutRequest());

  const Content = () => (
    <div>
      <h3 style={{ marginBottom: '10px' }}> Logout</h3>
      <p>Are you sure you want to logout now?</p>
    </div>
  );

  return (
    <div>
      <CustomizeModal
        open={logoutVisible}
        handleClose={closeModal}
        callback={callback}
        content={Content}
      />
      <CustomizeButton
        size="small"
        title="Account"
        type="button"
        color="neutral"
        variant="text"
        endIcon={open ? <ArrowDropUpOutlined /> : <ArrowDropDownOutlined />}
        onClick={handleOpen}
        //
      />
      <Popover
        elevation={1}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        //
      >
        <div className={style.popContent}>
          <Link onClick={handleClose} to="/account/info" className={style.item}>
            <span>Info</span>
            <ArrowRightAltOutlined />
          </Link>
          <Link onClick={handleClose} to="/account/address" className={style.item}>
            <span>Address</span>
            <ArrowRightAltOutlined />
          </Link>
          <Link onClick={handleClose} to="/account/orders" className={style.item}>
            <span>Orders</span>
            <ArrowRightAltOutlined />
          </Link>
          <Link onClick={handleClose} to="/account/notifications" className={style.item}>
            <span>Notifications</span>
            <ArrowRightAltOutlined />
          </Link>
          <Divider />
          <div className={style.end}>
            <CustomizeButton
              onClick={handleLogout}
              size="small"
              variant="text"
              color="neutral"
              type="button"
              title="Logout"
              endIcon={<LogoutOutlined />}
            />
          </div>
        </div>
      </Popover>
    </div>
  );
};

const HeaderNav = ({ mobileNavShow, setMobileNavShow }) => {
  const { cartList } = useSelector((state) => state.cartReducer);

  const dispatch = useDispatch();

  const openCart = () => dispatch(showCart());

  const onClose = () => setMobileNavShow(false);
  const onOpen = () => setMobileNavShow(true);

  return (
    <div className={style.headerNav}>
      <MobileNav setMobileNavShow={setMobileNavShow} open={mobileNavShow} onClose={onClose} />
      <div className={style.onlyMd}>
        <AccountComponent />
      </div>

      <Cart />

      <span onClick={openCart} className={style.icon}>
        <Badge badgeContent={cartList.length} color="primary">
          <ShoppingCartOutlined />
        </Badge>
      </span>

      <div className={style.onlySm}>
        <IconButton onClick={onOpen}>
          <MenuOutlined />
        </IconButton>
      </div>
    </div>
  );
};

const Header = () => {
  const scrollUp = useScrollingUp();

  const [mobileNavShow, setMobileNavShow] = useState(false);

  return (
    <header className={`${style.header} ${scrollUp && style.up}`}>
      <div className={`wrapContainer`}>
        <div className={style.container}>
          <div className={style.firstItem}>
            <Link className={style.title} to="/">
              <h1>Bestbuy</h1>
            </Link>
            <div className={style.onlyMd}>
              <HeaderCategories setMobileNavShow={setMobileNavShow} />
            </div>
          </div>
          <div className={style.md}>
            <SearchBar />
          </div>
          <HeaderNav mobileNavShow={mobileNavShow} setMobileNavShow={setMobileNavShow} />
        </div>
        <div className={style.sm}>
          <SearchBar />
        </div>
      </div>
    </header>
  );
};

export default Header;
