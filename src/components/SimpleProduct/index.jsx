import React, { useEffect, useState } from 'react';
import ProductImages from '../ProductImages';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import CustomizeButton from '../CustomizeButton';
import { AddOutlined } from '@mui/icons-material';
import { Rating } from '@mui/material';
import { useDispatch } from 'react-redux';
import { addToCart } from '../../redux/cartSlice';
import axiosClient from '../../libs/axiosClient';
import { addNoti } from '../../redux/notiSlice/slice';

const Info = ({ product }) => {
  const { title, price } = product;

  const dispatch = useDispatch();

  const onAddCart = () => {
    dispatch(addToCart({ product }));
  };

  const [rating, setRating] = useState(0);
  const getRating = async () => {
    try {
      const res = await axiosClient({
        url: `/reviews/${product._id}`,
        method: 'GET'
      });
      const { reviews } = res.data;
      const ratingValue =
        reviews.length > 0
          ? reviews.map((el) => el.rating).reduce((a, b) => (a += b)) / reviews.length
          : 0;
      setRating(ratingValue);
    } catch (err) {
      dispatch(addNoti(err.response.data.error));
    }
  };

  useEffect(() => {
    getRating();
  }, [product]);

  return (
    <div className={style.info}>
      <h3 className={style.title}>{title}</h3>
      <p className={style.price}>{price}$</p>
      <div className={style.rating}>
        <Rating value={rating} readOnly />
      </div>

      <CustomizeButton
        onClick={onAddCart}
        title="Add to cart"
        endIcon={<AddOutlined />}
        size="xl"
        variant="contained"
        color="primary"
      />
    </div>
  );
};

Info.propTypes = {
  product: PropTypes.object,
  changeVariant: PropTypes.func,
  currentVariant: PropTypes.object
};

const SimpleProduct = ({ product }) => {
  const { images, title, price } = product;
  return (
    <div className={style.container}>
      <ProductImages images={images} />
      <Info product={product} />
    </div>
  );
};

SimpleProduct.propTypes = {
  product: PropTypes.object
};

export default SimpleProduct;
