import React from 'react';
import PropTypes from 'prop-types';
import { FormControl, FormHelperText, InputBase, MenuItem, Select, styled } from '@mui/material';
import style from './style.module.scss';

const BootstrapInput = styled(InputBase)(({ theme, size, error }) => ({
  'label + &': {
    marginTop: theme.spacing(3)
  },
  '& .MuiInputBase-input': {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: `1px solid ${error ? theme.palette.danger.main : '#dfe3e8'}`,
    fontSize: size === 'md' ? '14px' : '13px',
    padding: size === 'md' ? '7px' : '5px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderColor: '#3391ff'
    }
  }
}));

const CustomizeSelect = ({
  value,
  onChange,
  error,
  disabled,
  size,
  options,
  multiple,
  notFull,
  placeholder
}) => {
  return (
    <FormControl fullWidth={!notFull} error={error ? true : false}>
      <Select
        className={`${style.select} ${style[size]}`}
        multiple={multiple}
        displayEmpty
        renderValue={(selected) => {
          if (!selected) return <em>{placeholder ? placeholder : ''}</em>;
          return selected;
        }}
        value={value === undefined ? '' : value}
        onChange={onChange}
        size={size}
        disabled={disabled}
        input={<BootstrapInput size={size} error={error ? true : false} />}>
        {options.map((item) => (
          <MenuItem sx={{ textTransform: 'capitalize' }} key={item} value={item}>
            {item}
          </MenuItem>
        ))}
      </Select>
      {error && <span className={style.error}>{error.message}</span>}
    </FormControl>
  );
};

CustomizeSelect.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  error: PropTypes.object,
  disabled: PropTypes.bool,
  size: PropTypes.string,
  options: PropTypes.array,
  multiple: PropTypes.bool,
  notFull: PropTypes.bool,
  placeholder: PropTypes.string
};

export default CustomizeSelect;
