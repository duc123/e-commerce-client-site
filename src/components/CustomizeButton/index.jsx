import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { LoadingButton } from '@mui/lab';

const CustomizeButton = ({
  type,
  title,
  variant,
  size,
  onClick,
  endIcon,
  color,
  disabled,
  loading,
  fullWidth,
  ariaDescribedby,
  loadingIndicator
}) => {
  return (
    <LoadingButton
      loadingIndicator={loadingIndicator}
      disableElevation={true}
      type={type}
      variant={variant}
      size={size}
      endIcon={endIcon}
      onClick={onClick}
      color={color}
      className={`${style.btn} ${style[size]}`}
      disabled={disabled}
      fullWidth={fullWidth}
      loading={loading}
      aria-describedby={ariaDescribedby}>
      {title}
    </LoadingButton>
  );
};

CustomizeButton.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
  variant: PropTypes.string,
  size: PropTypes.string,
  onClick: PropTypes.func,
  endIcon: PropTypes.element,
  color: PropTypes.string,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  fullWidth: PropTypes.bool,
  ariaDescribedby: PropTypes.any,
  loadingIndicator: PropTypes.string
};

export default CustomizeButton;
