import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';

const ProductImages = ({ images }) => {
  const [current, setCurrent] = useState(0);

  useEffect(() => {
    setCurrent(0);
  }, [images]);

  return (
    <div className={style.container}>
      <img className={style.mainImg} src={images[current]} alt="" />
      <div className={style.list}>
        {images.map((img, i) => (
          <img
            className={current === i ? style.active : ''}
            onClick={() => setCurrent(i)}
            src={img}
            key={i}
          />
        ))}
      </div>
    </div>
  );
};

ProductImages.propTypes = {
  images: PropTypes.array
};

export default ProductImages;
