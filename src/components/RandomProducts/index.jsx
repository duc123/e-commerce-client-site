//stric-mode
import { MoreOutlined } from '@mui/icons-material';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axiosClient from '../../libs/axiosClient';
import { setCurrent } from '../../redux/randomProductSlice';
import { getRandomProducts } from '../../redux/randomProductSlice/api';
import CustomizeButton from '../CustomizeButton';
import ProductList from '../ProductList';
import style from './style.module.scss';

const RandomProducts = () => {
  const perPage = 8;

  const dispatch = useDispatch();

  const { randomProducts, loading, total, limit, current } = useSelector(
    (state) => state.randomProductReducer
  );

  const getData = () => {
    if (current <= 1) {
      if (randomProducts.length <= 0) dispatch(getRandomProducts(current));
    } else {
      if (current >= limit) {
        if (randomProducts.length < total) {
          dispatch(getRandomProducts(current));
        }
      } else {
        if (randomProducts.length < perPage * current) dispatch(getRandomProducts(current));
      }
    }
  };

  useEffect(() => {
    getData();
  }, [current]);

  const handleGetData = () => {
    dispatch(setCurrent(current + 1));
  };

  return (
    <div className={style.container}>
      <div className={style.list}>
        <ProductList list={randomProducts} />
      </div>
      <CustomizeButton
        title={current >= limit ? 'You have reached limit' : 'More products'}
        variant="contained"
        size="md"
        type="button"
        color="primary"
        loading={loading}
        disabled={loading || current >= limit}
        endIcon={<MoreOutlined />}
        onClick={handleGetData}
      />
    </div>
  );
};

export default RandomProducts;
