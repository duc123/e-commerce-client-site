import React, { useEffect, useState } from 'react';
import ProductImages from '../ProductImages';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import { Alert, Rating } from '@mui/material';
import CustomizeButton from '../CustomizeButton';
import { AddOutlined, CheckOutlined } from '@mui/icons-material';
import { useDispatch } from 'react-redux';
import { addToCart } from '../../redux/cartSlice';
import axiosClient from '../../libs/axiosClient';

const Info = ({ product, changeVariant, currentVariant, rating }) => {
  const { attributes, title, variants, price } = product;

  const dispatch = useDispatch();

  const keys = attributes.map((el) => el.key);

  const handleChangeVariant = (key, val) => () => {
    const otherKey = keys.find((el) => el !== key);
    const newVariant = variants.find(
      (el) => el[otherKey] === currentVariant[otherKey] && el[key] === val
    );
    changeVariant(newVariant);
  };

  const onAdd = () => {
    dispatch(addToCart({ product, variant: currentVariant }));
  };

  return (
    <div className={style.info}>
      <h3 className={style.title}>{title}</h3>
      <p className={style.price}>{price}$</p>

      <Rating value={rating} readOnly />
      <div className={style.attributes}>
        {attributes.map((attr) => (
          <div className={style.attr} key={attr.key}>
            <p className={style.key}>{attr.key}</p>
            <div className={style.values}>
              {attr.values.map((val) => (
                <p
                  onClick={handleChangeVariant(attr.key, val)}
                  className={currentVariant[attr.key] === val ? style.active : ''}
                  key={val}>
                  <span>{val}</span>
                  {currentVariant[attr.key] === val && <CheckOutlined />}
                </p>
              ))}
            </div>
          </div>
        ))}
      </div>
      {currentVariant.stock < 1 && (
        <Alert sx={{ width: '100%', marginBottom: '20px' }} variant="outlined" severity="info">
          Out of stock
        </Alert>
      )}
      <CustomizeButton
        disabled={currentVariant.stock < 1}
        title="Add to cart"
        onClick={onAdd}
        endIcon={<AddOutlined />}
        size="xl"
        variant="contained"
        color="primary"
      />
    </div>
  );
};

Info.propTypes = {
  product: PropTypes.object,
  changeVariant: PropTypes.func,
  currentVariant: PropTypes.object
};

const ComplexProduct = ({ product }) => {
  const { variants } = product;

  const [currentVariant, setCurrentVariant] = useState(variants[0]);

  const changeVariant = (variant) => setCurrentVariant(variant);

  const mainImages = currentVariant.images.length > 0 ? currentVariant.images : product.images;

  const [rating, setRating] = useState(0);
  const getRating = async () => {
    try {
      const res = await axiosClient({
        url: `/reviews/${product._id}`,
        method: 'GET'
      });
      const { reviews } = res.data;
      const ratingValue =
        reviews.length > 0
          ? reviews.map((el) => el.rating).reduce((a, b) => (a += b)) / reviews.length
          : 0;
      setRating(ratingValue);
    } catch (err) {
      dispatch(addNoti(err.response.data.error));
    }
  };

  useEffect(() => {
    getRating();
  }, [product]);

  return (
    <div className={style.container}>
      <ProductImages images={mainImages} />
      <Info
        rating={rating}
        product={product}
        changeVariant={changeVariant}
        currentVariant={currentVariant}
      />
    </div>
  );
};

ComplexProduct.propTypes = {
  product: PropTypes.object
};

export default ComplexProduct;
