import React, { useEffect, useState } from 'react';
import axiosClient from '../../libs/axiosClient';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import ReviewItem from '../ReviewItem';
import CustomizeButton from '../CustomizeButton';
import { AddOutlined } from '@mui/icons-material';
import AddReview from '../AddReview';

const ReviewList = ({ reviews }) => {
  return (
    <div className={style.list}>
      {reviews.map((el) => (
        <ReviewItem item={el} key={el._id} />
      ))}
    </div>
  );
};

ReviewList.propTypes = {
  reviews: PropTypes.array
};

const Reviews = ({ productId, addNewReview, reviews }) => {
  const [modalVisible, setModalVisible] = useState(false);

  const showModal = () => setModalVisible(true);
  const hideModal = () => setModalVisible(false);
  // const [reviews, setReviews] = useState([]);

  //   const fetchReviews = async () => {
  //     try {
  //       const res = await axiosClient({
  //         url: `/reviews/${productId}`,
  //         method: 'GET'
  //       });
  //       setReviews(res.data.reviews);
  //     } catch (err) {
  //       console.log(err);
  //     }
  //   };

  //   const addNewReview = (review) => {
  //     setReviews([...reviews, review]);
  //   };

  //   useEffect(() => {
  //     if (productId) fetchReviews();
  //   }, [productId]);

  return (
    <div className={style.container}>
      <ReviewList reviews={reviews} />
      <AddReview
        productId={productId}
        open={modalVisible}
        hideModal={hideModal}
        addNewReview={addNewReview}
      />
      <CustomizeButton
        onClick={showModal}
        endIcon={<AddOutlined />}
        title="Add a review"
        variant="outlined"
        size="small"
        type="button"
      />
    </div>
  );
};

Reviews.propTypes = {
  productId: PropTypes.string,
  addNewReview: PropTypes.func,
  reviews: PropTypes.array
};

export default Reviews;
