import React, { useEffect, useState } from 'react';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import CustomizeButton from '../CustomizeButton';
import { AddOutlined, DetailsOutlined, VisibilityOutlined } from '@mui/icons-material';
import { IconButton, Rating, Tooltip } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { addToCart, updateQuantity } from '../../redux/cartSlice';
import axiosClient from '../../libs/axiosClient';
import { addNoti } from '../../redux/notiSlice/slice';
import { formatCurrency } from '../../utils/price';

const ProductItem = ({ item }) => {
  const { title, variants, images, price, _id: id, productType } = item;

  const { cartList } = useSelector((state) => state.cartReducer);

  const [rating, setRating] = useState(0);

  const dispatch = useDispatch();

  const onAddCart = () => {
    dispatch(addToCart({ product: item }));
  };

  const getRating = async () => {
    try {
      const res = await axiosClient({
        url: `/reviews/${id}`,
        method: 'GET'
      });
      const { reviews } = res.data;
      const ratingValue =
        reviews.length > 0
          ? reviews.map((el) => el.rating).reduce((a, b) => (a += b)) / reviews.length
          : 0;
      setRating(ratingValue);
    } catch (err) {
      dispatch(addNoti(err.response.data.error));
    }
  };

  useEffect(() => {
    getRating();
  }, [id]);

  return (
    <Link to={`/products/single/${id}`}>
      <div className={style.item}>
        <div className={style.imgContainer}>
          <img className={style.img} src={images[0]} alt={id} />
        </div>
        <div className={style.testContainer}>
          <p className={style.title}>{title}</p>
          <Rating name="" value={rating} readOnly size="small" />
          <div className={style.action}>
            <p className={style.price}>{formatCurrency(price)} $</p>
          </div>
        </div>
      </div>
    </Link>
  );
};

ProductItem.propTypes = {
  item: PropTypes.object
};

export default ProductItem;
