import { SearchOutlined } from '@mui/icons-material';
import { ButtonBase, InputBase } from '@mui/material';
import React, { useState } from 'react';
import CustomizeModal from '../CustomizeModal';

import style from './style.module.scss';

const SearchBar = () => {
  const [modal, setModal] = useState(false);

  const closeModal = () => setModal(false);

  const openModal = () => setModal(true);

  const Content = () => {
    return <p>Search function is not yet ready.</p>;
  };

  return (
    <>
      <CustomizeModal content={Content} handleClose={closeModal} open={modal} />
      <div className={style.container}>
        <InputBase
          className={style.input}
          placeholder="Search"
          startAdornment={<SearchOutlined />}
        />
        <ButtonBase onClick={openModal} className={style.button}>
          Search
        </ButtonBase>
      </div>
    </>
  );
};

export default SearchBar;
