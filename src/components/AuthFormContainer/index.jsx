import React from 'react';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import AuthHeader from '../AuthHeader';

const AuthFormContainer = ({ children }) => {
  return (
    <div className={style.auth}>
      <AuthHeader />
      <div className={style.container}>
        <div className={style.box}>{children}</div>
      </div>
    </div>
  );
};

AuthFormContainer.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired
};

export default AuthFormContainer;
