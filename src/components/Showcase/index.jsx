import React from 'react';
import style from './style.module.scss';

import Sofa1 from '../../assets/images/showCase/sofa_1.webp';
import Two from '../../assets/images/showCase/two.webp';
import Three from '../../assets/images/showCase/three.webp';

import Four from '../../assets/images/showCase/four.webp';

import Five from '../../assets/images/showCase/five.webp';

import { Link } from 'react-router-dom';
import CustomizeButton from '../CustomizeButton';
import { ArrowRightAltOutlined } from '@mui/icons-material';
import { Chip } from '@mui/material';

const ShowCaseList = () => {
  return (
    <div className={style.list}>
      <div className={style.showCaseItem}>
        <Chip size="small" color="error" label="13% off" variant="filled" className={style.off} />
        <div className={style.textContainer}>
          <h1>Furniture</h1>
          <Link to="/products/condition?c=furniture">
            <CustomizeButton
              title="Shop now"
              endIcon={<ArrowRightAltOutlined />}
              variant="outlined"
              color="primary"
              size="medium"
            />
          </Link>
        </div>
        <img src={Sofa1} alt="" />
      </div>
      <div className={style.showCaseItem}>
        <Chip size="small" color="error" label="7% off" variant="filled" className={style.off} />

        <div className={style.textContainer}>
          <h1>Beauty</h1>
          <Link to="/products/condition?c=beauty">
            <CustomizeButton
              title="Shop now"
              endIcon={<ArrowRightAltOutlined />}
              variant="outlined"
              color="primary"
              size="medium"
            />
          </Link>
        </div>
        <img src={Two} alt="" />
      </div>
      <div className={style.showCaseItem}>
        <Chip size="small" color="error" label="46% off" variant="filled" className={style.off} />

        <div className={style.textContainer}>
          <h1>Glasses</h1>
          <Link to="/products/condition?subC=glasses">
            <CustomizeButton
              title="Shop now"
              endIcon={<ArrowRightAltOutlined />}
              variant="outlined"
              color="primary"
              size="medium"
            />
          </Link>
        </div>
        <img src={Three} alt="" />
      </div>
      <div className={style.showCaseItem}>
        <div className={style.textContainer}>
          <Chip size="small" color="error" label="20% off" variant="filled" className={style.off} />

          <h1>Watches</h1>
          <Link to="/products/condition?subC=watch">
            <CustomizeButton
              title="Shop now"
              endIcon={<ArrowRightAltOutlined />}
              variant="outlined"
              color="primary"
              size="medium"
            />
          </Link>
        </div>
        <img src={Four} alt="" />
      </div>
      <div className={style.showCaseItem}>
        <div className={style.textContainer}>
          <Chip size="small" color="error" label="50% off" variant="filled" className={style.off} />

          <h1>Camera</h1>
          <Link to="/products/condition?subC=camera">
            <CustomizeButton
              title="Shop now"
              endIcon={<ArrowRightAltOutlined />}
              variant="outlined"
              color="primary"
              size="medium"
            />
          </Link>
        </div>
        <img src={Five} alt="" />
      </div>
    </div>
  );
};

const ShowCase = () => {
  return (
    <div className={style.showCase}>
      <ShowCaseList />
    </div>
  );
};

export default ShowCase;
