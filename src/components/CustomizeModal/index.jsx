import { IconButton, Modal } from '@mui/material';
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { CloseOutlined } from '@mui/icons-material';
import CustomizeButton from '../CustomizeButton';

const CustomizeModal = ({
  open,
  handleClose,
  content: Content,
  size = 'sm',
  callback,
  noFooter
}) => {
  const [loading, setLoading] = useState(false);
  const handleClick = async () => {
    if (callback) {
      setLoading(true);
      await callback();
      setLoading(false);
    }
    handleClose();
  };

  useEffect(() => {
    return () => setLoading(false);
  }, []);

  return (
    <Modal onClose={handleClose} open={open}>
      <div className={`${style.container} ${style[size]}`}>
        <div className={style.header}>
          <IconButton onClick={handleClose} size="small">
            <CloseOutlined />
          </IconButton>
        </div>
        <div className={style.body}>
          <Content />
        </div>
        {!noFooter && (
          <div className={style.footer}>
            {callback && (
              <CustomizeButton
                variant="text"
                title="Cancel"
                color="neutral"
                type="button"
                onClick={handleClose}
              />
            )}
            <CustomizeButton
              loading={loading}
              title="Ok"
              color="primary"
              variant="contained"
              type="button"
              size="large"
              onClick={handleClick}
            />
          </div>
        )}
      </div>
    </Modal>
  );
};

CustomizeModal.propTypes = {
  open: PropTypes.bool,
  handleClose: PropTypes.func,
  //   content: PropTypes.element,
  size: PropTypes.string,
  callback: PropTypes.func,
  noFooter: PropTypes.bool
};

export default CustomizeModal;
