import React from 'react';
import style from './style.module.scss';
import NotFoundImg from '../../assets/images/notFound/notFound.png';
import { Link, useNavigate } from 'react-router-dom';
import CustomizeButton from '../CustomizeButton';

const NotFound = () => {
  const navigate = useNavigate();

  const goBack = () => {
    navigate(-1);
  };

  return (
    <div className={style.notFound}>
      <h1>Page not found</h1>
      <img src={NotFoundImg} />
      <div className={style.flex}>
        <CustomizeButton
          size="large"
          title="Go back"
          color="error"
          variant="outlined"
          type="button"
          onClick={goBack}
        />
        <Link to="/">
          <CustomizeButton
            size="large"
            title="Go Home"
            color="error"
            variant="contained"
            type="button"
          />
        </Link>
      </div>
    </div>
  );
};

export default NotFound;
