import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { InputBase } from '@mui/material';

const CustomizeInput = ({
  label,
  value,
  onChange,
  error,
  disabled,
  size,
  type,
  multiline,
  rows,
  fullWidth,
  placeholder,
  endAdornment
}) => {
  return (
    <div className={style.control}>
      {label && <label>{label}</label>}
      <InputBase
        multiline={multiline}
        className={`${style.input} ${error && style.inputError} ${style[size]}`}
        value={value === undefined ? '' : value}
        onChange={onChange}
        disabled={disabled}
        size={size}
        type={type}
        rows={rows}
        fullWidth={fullWidth}
        placeholder={placeholder}
        endAdornment={endAdornment}
      />
      {error && <small className={style.error}>{error.message}</small>}
    </div>
  );
};

CustomizeInput.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  error: PropTypes.object,
  disabled: PropTypes.bool,
  size: PropTypes.string,
  type: PropTypes.string,
  multiline: PropTypes.bool,
  rows: PropTypes.number,
  fullWidth: PropTypes.bool,
  placeholder: PropTypes.string,
  endAdornment: PropTypes.element
};

export default CustomizeInput;
