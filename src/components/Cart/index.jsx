import { Drawer, IconButton } from '@mui/material';
import React, { useEffect } from 'react';
import style from './style.module.scss';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { ArrowRightAltOutlined, CloseOutlined, ShoppingCartOutlined } from '@mui/icons-material';
import EmptyImg from '../../assets/images/cart/empty.png';
import CustomizeButton from '../CustomizeButton';
import CartItem from '../CartItem';
import { Link } from 'react-router-dom';
import { hideCart, showCart } from '../../redux/cartSlice';

const CartList = ({ cartList, onHide }) => {
  return (
    <div className={style.listContainer}>
      <div className={style.scroll}>
        <div className={style.list}>
          {cartList.map((item, idx) => (
            <CartItem key={idx} item={item} />
          ))}
        </div>
      </div>
      <div className={style.actions}>
        <Link onClick={onHide} to="/carts">
          <CustomizeButton
            endIcon={<ArrowRightAltOutlined />}
            title="Checkout now"
            fullWidth
            variant="contained"
            color="primary"
          />
        </Link>
      </div>
    </div>
  );
};

CartList.propTypes = {
  cartList: PropTypes.array,
  hideCart: PropTypes.func
};

const Empty = () => {
  return (
    <div className={style.empty}>
      <img src={EmptyImg} alt="" />
      <div>
        <p>Your cart is empty</p>
        <p>Start shopping</p>
      </div>
    </div>
  );
};

const Cart = () => {
  const { cartList, cartVisible } = useSelector((state) => state.cartReducer);

  const dispatch = useDispatch();

  const onHide = () => dispatch(hideCart());

  return (
    <Drawer open={cartVisible} onClose={onHide} anchor="right">
      <div className={style.container}>
        <div className={style.header}>
          <div className={style.left}>
            <ShoppingCartOutlined />
            <span>{cartList.length} item</span>
          </div>
          <IconButton size="small" onClick={onHide}>
            <CloseOutlined />
          </IconButton>
        </div>
        <div className={style.main}>
          {cartList.length > 0 ? <CartList onHide={onHide} cartList={cartList} /> : <Empty />}
        </div>
      </div>
    </Drawer>
  );
};

Cart.propTypes = {};

export default Cart;
