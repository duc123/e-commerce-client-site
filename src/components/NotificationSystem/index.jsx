import { CloseOutlined } from '@mui/icons-material';
import { IconButton } from '@mui/material';
import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { forceLogout } from '../../redux/loginSlice';
import { addNoti, removeNoti } from '../../redux/notiSlice/slice';
import CustomizeButton from '../CustomizeButton';
import style from './style.module.scss';

const NotificationSystem = () => {
  const notiRef = useRef(null);

  const { message } = useSelector((state) => state.notiReducer);

  const showNoti = () => {
    notiRef.current.classList.add(style.active);
  };

  const hideNoti = () => {
    notiRef.current.classList.remove(style.active);
  };

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const onHide = () => {
    dispatch(removeNoti());
  };

  useEffect(() => {
    if (message) {
      if (message === 'Unauthenticated') {
        hideNoti();
        dispatch(forceLogout());
        navigate('/login');
      } else {
        hideNoti();
        const timeout = setTimeout(() => {
          showNoti();
        }, 200);
        return () => clearTimeout(timeout);
      }
    } else {
      hideNoti();
    }
  }, [message]);

  return (
    <>
      <div className={style.container} ref={notiRef}>
        <p>{message}</p>
        <IconButton onClick={onHide} className={style.icon} size="small">
          <CloseOutlined />
        </IconButton>
      </div>
    </>
  );
};

export default NotificationSystem;
