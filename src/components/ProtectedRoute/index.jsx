import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate, Outlet } from 'react-router-dom';
import AppContainer from '../AppContainer';

const ProtectedRoute = () => {
  const { authToken } = useSelector((state) => state.loginReducer);

  return authToken ? (
    <AppContainer>
      <Outlet />
    </AppContainer>
  ) : (
    <Navigate replace to="/login" />
  );
};

export default ProtectedRoute;
