import React, { useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import AccountSideBar from '../AccountSideBar';
import style from './style.module.scss';

const Account = () => {
  return (
    <div className="wrapContainer pTop">
      <h2>Account</h2>
      <div className={style.container}>
        <AccountSideBar />
        <div>
          <Outlet />
        </div>
      </div>
    </div>
  );
};

export default Account;
