import React, { useState } from 'react';
import Popover from '@mui/material/Popover';
import CustomizeButton from '../CustomizeButton';
import PropTypes from 'prop-types';

import style from './style.module.scss';
import { ArrowDropDownOutlined, ArrowDropUpOutlined } from '@mui/icons-material';
import { useSelector } from 'react-redux';
import { Collapse } from '@mui/material';
import { Link } from 'react-router-dom';

const CategoryItem = ({ category, onClose }) => {
  const { title, subs } = category;

  return (
    <div className={style.cItem}>
      <div className={style.block}>
        <Link onClick={onClose} className={style.title} to={`/products/condition?c=${title}`}>
          {title}
        </Link>
      </div>
      {/* <Collapse in={show}> */}
      <div className={style.subs}>
        {subs.map((sub) => (
          <Link
            onClick={onClose}
            key={sub}
            className={style.sub}
            to={`/products/condition?subC=${sub}`}>
            {sub}
          </Link>
        ))}
      </div>
      {/* </Collapse> */}
    </div>
  );
};

CategoryItem.propTypes = {
  category: PropTypes.object
};

const CategoriesDropDown = ({ onClose }) => {
  const { categories } = useSelector((state) => state.categoryReducer);

  return (
    <div className={style.popOver}>
      {categories.map((category) => (
        <CategoryItem onClose={onClose} key={category._id} category={category} />
      ))}
    </div>
  );
};

export default function HeaderCategories({ setMobileNavShow }) {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setMobileNavShow(false);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div>
      <CustomizeButton
        ariaDescribedby={id}
        onClick={handleClick}
        title="Categories"
        size="small"
        type="button"
        color="neutral"
        endIcon={open ? <ArrowDropUpOutlined /> : <ArrowDropDownOutlined />}
      />
      <Popover
        elevation={1}
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}>
        <CategoriesDropDown onClose={handleClose} />
      </Popover>
    </div>
  );
}
