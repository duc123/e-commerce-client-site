import React, { useEffect, useState } from 'react';
import CustomizeModal from '../CustomizeModal';
import PropTypes from 'prop-types';
import style from './style.module.scss';

import { yupResolver } from '@hookform/resolvers/yup';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';
import CustomizeButton from '../CustomizeButton';
import CustomizeInput from '../CustomizeInput';
import { Rating } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import axiosClient from '../../libs/axiosClient';
import { addNoti } from '../../redux/notiSlice/slice';

const AddReview = ({ open, hideModal, productId, addNewReview }) => {
  const ratings = ['Terrible', 'Bad', 'Normal', 'Good', 'Exellent'];
  const { authToken } = useSelector((state) => state.loginReducer);

  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();

  const schema = yup.object().shape({
    content: yup.string().required(),
    rating: yup.number().required().nullable()
  });

  const { handleSubmit, control, reset, setValue } = useForm({
    resolver: yupResolver(schema)
  });

  const submit = (data) => {
    if (!authToken) {
      dispatch(addNoti('You have to login in order to add review'));
      return;
    }
    const { content, rating } = data;
    const review = { productId, content, rating, uid: authToken };
    addReview(review);
  };

  const addReview = async (data) => {
    setLoading(true);
    try {
      const res = await axiosClient({
        url: '/reviews',
        data: data,
        method: 'POST'
      });
      const { review } = res.data;
      addNewReview(review);
      hideModal();
    } catch (err) {
      dispatch(addNoti(err.response.data.error));
    }
    setLoading(false);
  };

  useEffect(() => {
    reset();
  }, [open]);

  const Content = () => (
    <form onSubmit={handleSubmit(submit)} className={style.container}>
      <div className={style.block}>
        <span className={style.label}>Rating</span>
        <Controller
          defaultValue={null}
          render={({ field: { value, onChange }, fieldState: { error } }) => {
            return (
              <div>
                <Rating value={+value} onChange={onChange} />
                {error && <small className={style.error}>{error.message}</small>}
              </div>
            );
          }}
          name="rating"
          control={control}
        />{' '}
      </div>
      <div className={style.block}>
        <Controller
          render={({ field: { value, onChange }, fieldState: { error } }) => (
            <CustomizeInput
              placeholder="Add review"
              value={value}
              onChange={onChange}
              multiline
              size="lg"
              rows={5}
              label="Review"
              fullWidth
              error={error}
            />
          )}
          name="content"
          control={control}
        />
      </div>

      <div className={style.footer}>
        <CustomizeButton
          variant="text"
          title="Cancel"
          color="neutral"
          type="button"
          onClick={hideModal}
        />
        <CustomizeButton
          loading={loading}
          disabled={loading}
          title="Ok"
          color="primary"
          variant="contained"
          type="submit"
        />
      </div>
    </form>
  );

  return (
    <CustomizeModal
      open={open}
      handleClose={hideModal}
      content={Content}
      size="lg"
      noFooter={true}
      //
    />
  );
};

AddReview.propTypes = {
  open: PropTypes.bool,
  hideModal: PropTypes.func,
  productId: PropTypes.string,
  addNewReview: PropTypes.func
};

export default AddReview;
