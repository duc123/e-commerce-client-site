import React from 'react';
import { Link } from 'react-router-dom';
import style from './style.module.scss';
import FacebookIcon from '@mui/icons-material/Facebook';
import { Instagram, Twitter, YouTube } from '@mui/icons-material';
import { IconButton } from '@mui/material';
const Icons = () => {
  const getIcon = (name) => {
    switch (name) {
      case 'in':
        return <Instagram />;
      case 'tw':
        return <Twitter />;
      case 'you':
        return <YouTube />;
      default:
        return <FacebookIcon />;
    }
  };

  const icons = ['fb', 'in', 'tw', 'you'];
  return icons.map((el) => (
    <IconButton className={style.icon} key={el}>
      {getIcon(el)}
    </IconButton>
  ));
};

const List = ({ list }) => {
  return list.map((item) => (
    <div key={item.title}>
      <h3>{item.title}</h3>
      {item.items.map((el) => (
        <Link className={style.linkItem} to={`/${el}`} key={el}>
          {el}
        </Link>
      ))}
    </div>
  ));
};

const Footer = () => {
  const list = [
    {
      title: 'About us',
      items: ['Careers', 'Our store', 'Our care', 'Terms and conditions', 'Privacy policy']
    },
    {
      title: 'Customer care',
      items: [
        'Care center',
        'How to buy',
        'Track order',
        'Corporate and bulk purchasing',
        'Return and refund'
      ]
    }
  ];

  return (
    <footer className={style.footer}>
      <div className={`${style.container} wrapContainer`}>
        <div className={style.gridItem}>
          <Link className={style.title} to="/">
            <h3>Bestbuy</h3>
          </Link>
          <p className={style.text}>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed sapiente, recusandae
            voluptate doloribus natus perferendis!
          </p>
        </div>
        <List list={list} />
        <div className={style.gridItem}>
          <h3>Contact us</h3>
          <p className={style.text}>
            70 Washington Square South, New York, NY 10012, United States
          </p>
          <p className={style.text}>Email: luuminhduc00@gmail.com</p>
          <p className={style.text}>Phone: 0383172117</p>
          <Icons />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
