import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import ProductItem from '../ProductItem';

const ProductList = ({ list, sm }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(list);
  }, [list]);

  return (
    <div className={`${style.list} ${sm ? style.sm : ''}`}>
      {data.length > 0 ? data.map((item) => <ProductItem key={item._id} item={item} />) : ''}
    </div>
  );
};

ProductList.propTypes = {
  list: PropTypes.array
};

export default ProductList;
