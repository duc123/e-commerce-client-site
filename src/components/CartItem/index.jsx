import React from 'react';
import PropTypes from 'prop-types';
import style from './style.module.scss';
import { IconButton } from '@mui/material';
import { AddOutlined, CloseOutlined, RemoveOutlined } from '@mui/icons-material';
import CustomizeButton from '../CustomizeButton';
import { useDispatch } from 'react-redux';
import { removeCartItem, updateQuantity } from '../../redux/cartSlice';
import { formatCurrency } from '../../utils/price';

const CartItem = ({ item }) => {
  const {
    price,
    title,
    quantity,
    productType,
    image,
    variant,
    attributes,
    productId: id,
    stock
  } = item;

  const dispatch = useDispatch();

  const getAttr = () => {
    const keys = attributes.map((el) => el.key);
    return `(${keys.map((k) => `${variant[k]}`)})`;
  };

  const onUpdateQty = (isAdd) => () => {
    if (variant) {
      const productInfo = { productType, id };
      dispatch(updateQuantity({ productInfo, isAdd, variant }));
    } else {
      const productInfo = { id, stock, productType };
      dispatch(updateQuantity({ productInfo, isAdd }));
    }
  };

  const onRemove = () => {
    dispatch(removeCartItem({ productType, productId: id, variantId: variant?.id }));
  };

  return (
    <div className={style.item}>
      <div className={style.left}>
        <img className={style.img} src={image} />
        <div className={style.textContainer}>
          <p>
            <span className={style.title}>{title}</span>
            {variant && <span className={style.attr}>{getAttr()}</span>}
          </p>
          <p className={style.price}>
            {formatCurrency(price * quantity)}$
            <span className={style.per}>
              {price}$ * {quantity}
            </span>
          </p>
        </div>
      </div>
      <div className={style.right}>
        <IconButton onClick={onRemove} className={style.closeIcon} size="small">
          <CloseOutlined sx={{ fontSize: '15px' }} />
        </IconButton>
        <div className={style.quantityContainer}>
          <IconButton onClick={onUpdateQty(true)} size="small" color="primary">
            <AddOutlined />
          </IconButton>
          <span className={style.quantity}>{quantity}</span>
          <IconButton onClick={onUpdateQty(false)} size="small" color="primary">
            <RemoveOutlined />
          </IconButton>{' '}
        </div>
      </div>
    </div>
  );
};

CartItem.propTypes = {
  item: PropTypes.object
};

export default CartItem;
