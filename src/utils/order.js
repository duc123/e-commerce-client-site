import axiosClient from '../libs/axiosClient';

export const addNewOrder = async (data) => {
  try {
    const res = await axiosClient({
      url: '/orders',
      method: 'POST',
      data
    });
    return res.data;
  } catch (err) {
    throw err.response.data.error;
  }
};
