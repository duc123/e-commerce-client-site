export const calTemp = (list) => {
  return list.length > 0
    ? list.map((el) => +el.price * +el.quantity).reduce((acc, el) => (acc += el))
    : 0;
};

export const calTax = (price) => {
  const result = (price * 5) / 100;
  return result;
};
export const calTotalTax = (list) => {
  return list.map((el) => calTax(el.price)).reduce((a, b) => (a += b));
};

export const calDeliveryFee = () => {
  return 5;
};

export const formatCurrency = (number) => {
  const splited = number.toString().split('');

  const dotIndex = splited.findIndex((el) => el === '.');

  const firstHalf = dotIndex !== -1 ? splited.slice(0, dotIndex) : splited;

  const lastHalf = dotIndex !== -1 ? splited.slice(dotIndex) : [];

  for (let i = firstHalf.length - 3; i > 0; i -= 3) {
    firstHalf.splice(i, 0, ',');
  }

  const arr = [...firstHalf, ...lastHalf];

  const result = arr.length > 0 ? arr.reduce((a, b) => (a += b)) : 0;

  return result;
};
