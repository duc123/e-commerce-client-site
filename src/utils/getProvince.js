export const getCities = async () => {
  try {
    const res = await fetch('https://vapi.vnappmob.com/api/province');
    const data = await res.json();
    return data;
  } catch (err) {
    console.log(err);
  }
};

export const getDistricts = async (id) => {
  try {
    const res = await fetch(`https://vapi.vnappmob.com/api/province/district/${id}`);
    const data = await res.json();
    return data;
  } catch (err) {
    console.log(err);
  }
};

export const getWards = async (id) => {
  try {
    const res = await fetch(`https://vapi.vnappmob.com/api/province/ward/${id}`);
    const data = await res.json();
    return data;
  } catch (err) {
    console.log(err);
  }
};
